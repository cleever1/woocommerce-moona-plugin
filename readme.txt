===Cleever - Monetise Your Checkout ===
Contributors: cnguyen
Tags: payment, discount, checkout, credit card,  woocommerce, stripe, e-commerce, ecommerce, paypal, express checkout, payments pro, payment request, payment gateway, one click, online payment, stripe,  shopping Cart, extension, store, sales, sell, shop, cart, payments, coupon, voucher, conversion, rebate, promotion
Requires at least: 3.2.0
Requires PHP: 7.0
Tested up to: 6.4.1
Stable tag: 2.2.0
License: GPLv3 or later
License URI: [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html)
Cleever payment solution unlocks discounts for your customers, and at no cost to you!
== Description ==

Cleever allows any online e-commerce to monetise their website for free, while offering funded incentives to your cusotmers.

This free plugin provides 2 main features:

**Funded Incentives** - Our incentives, valued up to £15, are tailored to encourage repeat visits to your website. By offering these attractive rewards, you'll turn one-time buyers into loyal, returning customers, boosting your long-term engagement and growth.

**New Revenue Stream** - Cleever will pay you to show and offer the incentives to your customers, not only will the customer pay less, you will also increase your margin per order.

Cleever can be installed in a few clicks. No coding required. in less than 5 minutes.

= What will Cleever Do for you? =
1. **Boost Margin:** For any order where an incentive get's displayed, Cleever will increase your margin by paying you a net commission
2. **Increase Conversion:** Thanks to the offers and discounts more customers will check out, and the disocunts won't eat into your margin.
3. **Increase Repeat Business:** Our offers are designed to drive traffic back to your website, by offering discounts that Cleever pays for.
4. **Bigger Carts:** Getting a discount on their current or next order means customers are will add more to their cart, increasing thier basket value.
5. **Easy integration:** Getting started with Cleever is easy for all WooCommerce stores. Live in 5 minutes.

= Why will your shoppers love Cleever? =
1. **More Discounts:** Any shopper that takes a discount, will gain access to one of our services offering daily discounts and offers.
2. **More Items:** With Cleever's discounts your customers will buy more items, as they select the discounts we fund for them.


**How do I get Started?**
It only takes 5 minutes, to increase boost your margin and sales.
Follow these Steps:

**Sign-up to Cleever**
1. Go to [https://partners.cleever.com/register](https://partners.cleever.com/register)
2. Enter your details, and follow the steps

**Install the Cleever WooCommerce extension**
1. Connect to your WordPress account
2. On the side bar, click on "Plugins"-"Add new"
3. In the search bar, type Cleever
4. Once you found Cleever, click on "Install now"
5. When the plugin is installed, click on "Activate"
6. In your Wordpress Plugins > Installed Plugins > Cleever for WooCommerce settings, indicate your Cleever credentials. You can get your credentials by logging into on your Cleever merchant dashboard (http://partners.cleever.com)
7. In Plugins > Installed Plugins > Cleever for WooCommerce settings, check that Cleever is enabled, choose your payment settings and save changes
8. You are live!

== FAQ ==
= What is "Cleever - Monetise your Checkout"? =
Cleever offers a cost-free solution for online e-commerce platforms, not only allowing them to monetise their websites but also significantly boosting their profit margins. This innovative platform is specifically designed to cater to businesses seeking additional revenue streams, making it a valuable asset for sustained growth.

= What is the cost of "Cleever - Monetise your Checkout"? =
Our model relies on shopper memberships, and the best part is, Cleever doesn't impose any fees or charges to merchants. You have the freedom to download and try it at your convenience, putting you firmly in control of your e-commerce journey.

= Do I have to pay for the discounts you give to my customers? =
No! Cleever funds the discounts given to your customers, regardless is the discount is pre or post purchase.

= Does "Cleever - Monetise your Checkout" support both live mode and test modes? =
Certainly. Cleever provides a seamless transition between live and test modes with a simple toggle option available in the admin settings. This not only makes it easy for merchants to experiment but also allows them to download and preview how it would seamlessly integrate into their online store.

= Do you accept all merchants on Cleever? =
The following categories of businesses and business practices are restricted from using our partner Stripe Service (["Restricted Businesses"](https://stripe.com/gb/restricted-businesses)).
By registering with us, you are confirming that you will not use the Service to accept payments in connection with the following businesses, business activities, or business practices, unless you have received prior written approval from Cleever.

= What currency does Cleever support? =
Currently, Cleever only supports payments processed in GBP, get in touch to learn more about other currencies.

= In what countries can I use Cleever? =
Cleever is only available for merchants registered in the United Kingdom, at the moment. It will soon expand to the US, Canada, and Australia.

= Which payment method does Cleever support? =
By selling your product using Cleever, you can immediately accept purchases via:
- Visa
- Mastercard
- American Express
- Discover
- JCB

= What is the Merchant Dashboard? =
The Merchant Dashboard is the Cleever's order handling system for online stores implementing Cleever as a payment system. Here you can:
- View all orders having gone through Cleever Payments
- Receive reports which can be used for book-keeping
- Change your settings and company information

= I have changed my Organisation Number. What do I need to do? =
If you have made changes to your business, such as changing the structure as a sole trader to a limited company, you will need to complete a transfer of your agreement with Cleever

= Do I have a contact person? =
Yes, you will have a specific contact available to support you. Don't hesitate to contact our Merchant Support through phone or email for assistance.

== Screenshots ==

1. Cleever secure payment appears as any other payment method on your website. The payment form is integrated to checkout page to increase conversion.
2. Cleever secure payment with discount enables shoppers to directly benefit from a discount, funded by Cleever!!!
3. Access all your payments and payouts through a user firendly dashboard highlighting all the extra earnings made thanks to our payment solution.
4. Our secured payment solution is powered by Stripe (official partner), and supports all major card brands including American Express.

== Changelog ==
= 2.2.0 - 2023-12-05
* Add post-payment slot to enable new offer
= 2.1.0 - 2022-12-14 =
* Add shipping slot
* Add product slot
* Display new price when we tick the box in the integrated form
* Cleever payment can be activated alone
= 2.0.4 - 2022-01-28 =
* Optimize refund for m-payment
* Update error message on refund errors
* Add param to card-form url to identify the merchant
= 2.0.3 - 2021-11-11 =
* Optimize CRON to handle pending payment
* Optimize IPN to update trash order status
* Optimize the way to have the Cleever payment position
* Optimize the update of the Cleever checkout informations
= 2.0.2 - 2021-10-27 =
* Manage CSS and JS cache file system
= 2.0.1 - 2021-10-26 =
* Fix issue on test mode
= 2.0.0 - 2021-10-26 =
* New gateway to manage payments without discount
* Enable total and partial refunds from WooCommerce back-office
* Improve management of pending orders
= 1.0.0 - 2021-09-20 =
* Add plugin version information
* Save plugin status on CTA BO, (un)installation, (dev)activation
* Display discount at checkout, order confirmation, email order confirmation
* Add iframe mode
* Update banners once by 24h
* Default banners options set to 'On'
= 0.2.3 - 2021-07-11 =
* Display banners only when Cleever is enabled
* Add Amplitude information
* Remove Cleever tag on order creation
= 0.2.2 - 2021-02-25 =
* Fix : check if WC_Logger exists
= 0.2.1 - 2021-02-24 =
* Fix : manage wp_error messages
= 0.2.0 - 2021-02-22 =
* Allow display Cleever banners
= 0.1.0 - 2020-11-27 =
* First Release to add Cleever payment gateway