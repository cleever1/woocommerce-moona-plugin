<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Cleever Payment Gateway
*/
if ( ! class_exists( 'WC_Moona_Gateway' ) ) {
    class WC_Moona_Gateway extends WC_Payment_Gateway 
    {
        public static $log = false ;
        
        /**
         * Constructor for the gateway.
         */
        public function __construct()
        {   
            $this->id = 'discount-payment-moona';
            $this->icon = WCMOONA_BASE_URL . 'assets/images/checkout-secure-card-logos.svg';
            $this->title = __('CARD: £5 OFF - Secure payment', 'discount-payment-moona');
            $this->description = __('Your £5 welcome discount will be applied on the payment page', 'discount-payment-moona');
            $this->method_title = __( 'Cleever - Secure payment with discounts', 'discount-payment-moona' );
            $this->method_description = __( 'Offer welcome discount to all your shoppers.', 'discount-payment-moona' );
            $this->has_fields = false;

            // gateways can support subscriptions, refunds, saved payment methods and simple payments like here
            $this->supports = array( 'products', 'refunds' );

            // Define our settings that are then loaded with init_settings()
            $this->init_form_fields();

            // After init_settings() is called, we can load the settings
            $this->init_settings();
            $this->enabled = $this->get_option( 'enabled' );
            $this->enabled_main = $this->get_option( 'enabled_main' );
            $this->test_mode = $this->get_option( 'test_mode' );
            $this->cancel_url = wc_get_cart_url();
            $this->moona_sk_live = $this->get_option( 'moona_sk_live' );
            $this->moona_pk_live = $this->get_option( 'moona_pk_live' );
            $this->moona_sk_test = $this->get_option( 'moona_sk_test' );
            $this->moona_pk_test = $this->get_option( 'moona_pk_test' );

            // Refresh title and description
            $this->title = (!empty($this->get_option( '_moona_discount_checkout_title' ))) ? $this->get_option( '_moona_discount_checkout_title' ) : $this->title;
            $this->icon = (!empty($this->get_option( '_moona_discount_checkout_icon' ))) ? $this->get_option( '_moona_discount_checkout_icon' ) : $this->icon;
            $this->description = (!empty($this->get_option( '_moona_discount_checkout_description' ))) ? $this->get_option( '_moona_discount_checkout_description' ) : $this->description;

            // BANNERS
            // Font size needs to be controllable
            // Font color needs to be controllable
            // Background color needs to be controllable

            // Top banners
            $this->banner_top_shop_archives = $this->get_option( 'banner_top_shop_archives' ); // woocommerce_before_shop_loop
            $this->banner_top_product_page = $this->get_option( 'banner_top_product_page' ); // woocommerce_before_single_product
            $this->banner_top_cart_page = $this->get_option( 'banner_top_cart_page' ); // woocommerce_before_cart_table
            $this->banner_top_checkout_page = $this->get_option( 'banner_top_checkout_page' ); // woocommerce_before_checkout_form
            $this->banner_top_thankyou_page = $this->get_option( 'banner_top_thankyou_page' ); // woocommerce_thankyou

            // CTA banners
            $this->banner_above_cta_product_page = $this->get_option( 'banner_above_cta_product_page' ); // woocommerce_before_add_to_cart_form
            $this->banner_above_cta_cart_page = $this->get_option( 'banner_above_cta_cart_page' ); // woocommerce_proceed_to_checkout
            $this->banner_above_cta_checkout_page = $this->get_option( 'banner_above_cta_checkout_page' ); // woocommerce_review_order_before_payment
            $this->banner_above_cta_minicart = $this->get_option( 'banner_above_cta_minicart' ); // woocommerce_mini_cart_contents
            
            // mini cart
            $this->banner_square_cart_page = $this->get_option( 'banner_square_cart_page' ); // woocommerce_after_mini_cart
            // END BANNERS
            
            // Sets the callback/IPN URL
            $this->notify_url = add_query_arg( 'wc-api', 'WC_Moona_Gateway', home_url( '/' ) );

            // Set the hook to send payment completed information
            // http://website.com/wc-api/{$this->webhook_name}/
            $this->webhook_name = 'wc_moona_gateway';
            add_action( 'woocommerce_api_' . $this->webhook_name, array( $this, 'check_ipn_response' ) );
            
            // Action hook which save the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

            // Load css file
            add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts') );

            // Custom title here OR public function payment_fields() for the description ?
            if ( $this->is_available() && is_checkout() ) {
                $this->customize_checkout_infos();
            }

            add_action( 'woocommerce_receipt_'. $this->id, array( $this, 'display_iframe_on_checkout' ) );
            
            // Do action at the end for other plugin
            do_action( 'wc_moona.loaded' );
        }

        /**
         * Display the iframe
         */
        function display_iframe_on_checkout($order_id) {

            $order = wc_get_order($order_id);

            // IFRAME
            // Allow to go back when the payment is finished
            ?>
            <script>
                jQuery(function($) {
                    window.addEventListener("message", (event) => {                        
                        if (!event.origin.includes("secure-payment-ecommerce.com") && !event.origin.includes("secure-payment.moona.com") && !event.origin.includes("e-securepay.com"))
                            return;
                        
                        var data = event.data;
                        if (data.source !== "Moona" && data.source !== "Cleever") {
                            // console.log("Cleever discount: Wrong source");
                            return;
                        } else {
                            // console.log("Cleever DISCOUNT");

                            switch (data.action) {
                                case "open-new-tab":
                                    window.open(data.url, "_blank");
                                    break;
                                case "redirect":
                                    window.top.history.replaceState("", "", data.url);
                                    window.top.location.reload();
                                    break;
                                case "finish-moona-process":
                                    window.top.history.replaceState("", "", data.url);
                                    window.top.location.reload();
                                    break;
                                case "go-to-cart":
                                    window.top.history.replaceState("", "", "<?php echo wc_get_cart_url(); ?>");
                                    window.top.location.reload();
                                    break;
                                case "go-to-checkout":
                                    window.top.history.replaceState("", "", "<?php echo wc_get_checkout_url(); ?>");
                                    window.top.location.reload();
                                    break;
                                case "cancel":
                                    window.top.history.replaceState("", "", "<?php echo wc_get_checkout_url(); ?>");
                                    window.top.location.reload();
                                    break;
                                case "paid":
                                    // { has_moona_discount: true, order_id: XXX }
                                    let json = {
                                        action: 'amplitude_action',
                                        step: 'order_paid',
                                        order_id: data.payload.order_id,
                                        has_moona_discount: data.payload.has_moona_discount
                                    };
                                    $.post(ajax_object.ajax_url, json);
                                    break;
                                default:
                                    console.log("There is no response of Cleever discount API")
                            }
                        }
                    }, false);
                });
            </script>

            <div class="payment-popin-container" >
                <div>
                    <iframe
                        id="payment-form-iframe"
                        name="payment-iframe"
                        src="<?php echo $order->get_meta('_url_to_redirect'); ?>"
                        scrolling="yes"
                        frameborder="0"
                        border="0"
                        height="100%"
                        width="100%"
                    >
                    </iframe>
                </div>
            </div>
            <?php
        }

        function admin_enqueue_scripts() {
            // Load css file
            wp_enqueue_style('wc_moona_css', WCMOONA_BASE_URL . 'assets/css/moona.css', array(), WCMOONA_FILE_VERSION);
        }

        /**
         * Customize the checkout information according to WS
         */
        private function customize_checkout_infos() {

            // Update title and icon every 24h
            $options = get_option('woocommerce_discount-payment-moona_settings');
            
            if ( isset($options['_timestamp_moona_discount_checkout']) ) {
                if ( ($options['_timestamp_moona_discount_checkout'] + 24*60*60) > time() ) {
                    return;
                }
            }
            $options['_timestamp_moona_discount_checkout'] = time();
            
            $locale = get_locale();
            $total = WC()->cart->total;

            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'lang' => $locale, // en_GB
                'price' => $total // 54.50
            );

            if (isset($options['merchant_id'])) {
                $body['merchant_id'] = $options['merchant_id'];
            }

            $url = (( $this->test_mode === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT);
            $url .= 'ecommerce/checkout';

            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->test_mode === 'yes' ) ? $this->moona_sk_test : $this->moona_sk_live
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                $this->log( 'error on customize_checkout_infos() process : ' . print_r($response, true) );
            } else {
                $body = json_decode( $response['body'], true );
                $this->title = $body['title'];
                $this->icon = $body['icon'];
                $this->description= $body['description'];

                $options['_moona_discount_checkout_title'] = $this->title;
                $options['_moona_discount_checkout_icon'] = $this->icon;
                $options['_moona_discount_checkout_description'] = $this->description;
                update_option('woocommerce_discount-payment-moona_settings', $options);
            }
        }

        /**
         * Display message before the form if the keys are not filled in.
         */
        public function admin_options() 
        {
            if ( $this->is_empty_keys() ) {
                $mode = ( $this->test_mode === 'yes' ) ? 'staging.' : '';
                $merchanturl = 'https://'.$mode.'partners.cleever.com/';

                /* translators: %s: merchant url */
                $link = sprintf( wp_kses( __( 'You can find your keys in your <a href="%s" target="_blank">merchant dashboard</a>. Connect to the portal > Menu > Access Information', 'discount-payment-moona' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $merchanturl ) );

                echo  '<div class="wc-moona-error">
                    <p>' . __( 'You have to fill in the authentication keys fields to activate the payment gateway.', 'discount-payment-moona') . '</p>
                    <p>' . $link . '</p>
                </div>' ;
            }

            parent::admin_options();
        }

        private function webhook_exists($name) {
            // Récupérez tous les webhooks existants
            $data_store = WC_Data_Store::load('webhook');
            $webhooks = $data_store->search_webhooks();
            
            foreach ($webhooks as $webhook_id) {
                $item = new WC_Webhook($webhook_id);
                if ($name === $item->get_name()) {
                    return $item;
                }
            }
            return null;
        }

        private function generate_webhook_secret($length = 32) {
            // Caractères disponibles pour le secret
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
            $secret = '';
        
            // Génération du secret
            for ($i = 0; $i < $length; $i++) {
                $secret .= $characters[random_int(0, strlen($characters) - 1)];
            }
        
            return $secret;
        }

        private function generate_api_key($prefix) {
            // Générer une chaîne aléatoire de 40 caractères
            $random_part = $this->generate_webhook_secret(40);
        
            // Construire la clé API complète
            $api_key = $prefix. '_' . $random_part;
        
            return $api_key;
        }

        function delete_api_key($description) {
            global $wpdb;
        
            $table_name = $wpdb->prefix . 'woocommerce_api_keys';
        
            $query = $wpdb->prepare("DELETE FROM $table_name WHERE description = %s", $description);
        
            $delete = $wpdb->get_var($query);

            return $delete;
        }

        /**
         * Click to save the settings
         */
        public function process_admin_options() 
        {
            $post_data = $this->get_post_data();
            $sklive = wc_get_var( $post_data[$this->plugin_id . $this->id . '_moona_sk_live'], '' );
            $pklive = wc_get_var( $post_data[$this->plugin_id . $this->id . '_moona_pk_live'], '' );
            $sktest = wc_get_var( $post_data[$this->plugin_id . $this->id . '_moona_sk_test'], '' );
            $pktest = wc_get_var( $post_data[$this->plugin_id . $this->id . '_moona_pk_test'], '' );
            // Be careful, value is '1' and not 'yes' here
            $test_mode = wc_get_var( $post_data[$this->plugin_id . $this->id . '_test_mode'], '' );
            $enabled = wc_get_var( $post_data[$this->plugin_id . $this->id . '_enabled'], '' );
            $enabled_main = wc_get_var( $post_data[$this->plugin_id . $this->id . '_enabled_main'], '' );
            $enabled_shipping_slot = wc_get_var( $post_data[$this->plugin_id . $this->id . '_shipping_slot_enabled'], '' );
            $enabled_product_slot = wc_get_var( $post_data[$this->plugin_id . $this->id . '_product_slot_enabled'], '' );

            $old_enabled_main = $this->get_option( 'enabled_main' );

            if ( empty($sklive) || empty($pklive) || empty($sktest) || empty($pktest) ) {
                WC_Admin_Settings::add_error( __( 'You have to fill in the authentication keys fields to activate the payment gateway.', 'discount-payment-moona') );
                unset( $post_data[$this->plugin_id . $this->id . '_enabled'] );
            }

            if ( empty($sktest) || empty($pktest) ) {
                WC_Admin_Settings::add_error( __( 'You have to fill in the test authentication keys fields to activate the TEST mode.', 'discount-payment-moona') );
                unset( $post_data[$this->plugin_id . $this->id . '_test_mode'] );
            }

            // if ( $enabled !== '1' && $enabled_main === '1' ) {
            //     WC_Admin_Settings::add_error( __( 'Cleever payment can\'t be activated alone.', 'discount-payment-moona') );
            //     unset( $post_data[$this->plugin_id . $this->id . '_enabled_main'] );
            // }

            // Cleever payment is disabled by user, but shipping slot is still enabled => we disable shipping slot
            if ( $enabled_main !== '1' && $old_enabled_main === 'yes' && $enabled_shipping_slot === '1' ) {
                WC_Admin_Settings::add_error( __( 'We have automatically disabled the shipping slot because Cleever payment is disabled.', 'discount-payment-moona') );
                unset( $post_data[$this->plugin_id . $this->id . '_shipping_slot_enabled'] );
                $enabled_shipping_slot = '0';   
            }
            // Shipping slot is enabled by user, but Cleever payment is not enabled => we enable Cleever payment
            else if ( $enabled_shipping_slot === '1' && $enabled_main !== '1' ) {
                WC_Admin_Settings::add_error( __( 'We have automatically enabled Cleever payment because it is required by the shipping slot.', 'discount-payment-moona') );
                $post_data[$this->plugin_id . $this->id . '_enabled_main'] = '1';
                $enabled_main = '1';                
            }

            // Cleever payment is disabled by user, but product slot is still enabled => we disable product slot
            if ( $enabled_main !== '1' && $old_enabled_main === 'yes' && $enabled_product_slot === '1' ) {
                WC_Admin_Settings::add_error( __( 'We have automatically disabled the product slot because Cleever payment is disabled.', 'discount-payment-moona') );
                unset( $post_data[$this->plugin_id . $this->id . '_product_slot_enabled'] );
                $enabled_shipping_slot = '0';   
            }
            // Shipping slot is enabled by user, but Cleever payment is not enabled => we enable Cleever payment
            else if ( $enabled_product_slot === '1' && $enabled_main !== '1' ) {
                WC_Admin_Settings::add_error( __( 'We have automatically enabled Cleever payment because it is required by the product slot.', 'discount-payment-moona') );
                $post_data[$this->plugin_id . $this->id . '_enabled_main'] = '1';
                $enabled_main = '1';                
            }

            if ( get_option( 'moona_disable_cache_until_timestamp' ) !== false ) {
                update_option( 'moona_disable_cache_until_timestamp', time() + 3600 );
            } else {
                add_option( 'moona_disable_cache_until_timestamp', time() + 3600 );
            }

            /************************************************/
            /************** CREATE API KEYS *****************/
            /************************************************/
            $data = array();
            $description = 'API Key - WC - Discount';
            $generated_consumer_key = $this->generate_api_key('ck');

            $data = array(
                'description' => $description,
                'user_id' => get_current_user_id(),
                'permissions' => 'read_write',
                'consumer_key' => wc_api_hash($generated_consumer_key),
                'consumer_secret' => $this->generate_api_key('cs'),
            );

            $this->delete_api_key($description);
                
            global $wpdb;
            $wpdb->insert(
                $wpdb->prefix . "woocommerce_api_keys",
                array(
                    "user_id" => $data['user_id'],
                    "description" => $data['description'],
                    "permissions" => $data['permissions'],
                    "consumer_key"=> $data['consumer_key'],
                    "consumer_secret" => $data['consumer_secret'],
                    "truncated_key" => substr($data['consumer_secret'], -7)
                )
            );

            /************************************************/
            /************** CREATE WEBHOOK ******************/
            /************************************************/
            $webhook_url_suffix = ( $test_mode === '1' ) ? 'staging/handlePaidOrders-dev' : 'main/handlePaidOrders-prod';
            $webhook_url = 'https://toevk4xdp2.execute-api.eu-west-2.amazonaws.com/'.$webhook_url_suffix;

            // Create crypted keys to save on DB
            $webhook_secret = $this->generate_webhook_secret();

            $name = 'Order updated - WC - Discount';
            $topic = 'order.updated';
            // Save the webhook
            // Order updated (from processing to paid)
            // WS who create the key for the merchant
            $webhook_item = $this->webhook_exists($name);
            if ($webhook_item !== null) {
                // Le webhook existe déjà
                $webhook_item->set_secret($webhook_secret);
                $webhook_item->set_delivery_url($webhook_url);
                $webhook_item->save();
            } else {
                $webhook = new WC_Webhook();
                $webhook->set_name($name);
                // $webhook->set_user_id(get_current_user_id());
                $webhook->set_status('active');
                $webhook->set_secret($webhook_secret);
                $webhook->set_topic($topic);
                $webhook->set_delivery_url($webhook_url);
                $webhook->save();
            }

            // Save the merchant id
            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'webhook_secret' => $webhook_secret,
            );

            // Add API REST KEYS
            $body['api_consumer_key'] = array(
                'consumer_key' => $generated_consumer_key,
                'consumer_secret' => $data['consumer_secret'],
            );

            $merchantId = null;
            $url = (( $test_mode === '1' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT);
            $url .= 'ecommerce/merchant';

            $response = wp_remote_post( $url, array(
                                'method' => 'POST',
                                'timeout' => 20,
                                'headers' => array(
                                    'Content-Type' => 'application/json; charset=utf-8',
                                    'x-api-key' => ( $test_mode === '1' ) ? $sktest : $sklive
                                ),
                                'body' => json_encode($body)
                            )
                        );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
                $this->log( 'error on process_admin_options() process : ' . print_r($response, true) );
            } else {
                $body = json_decode( $response['body'], true );
                // Doesn't work
                // $post_data[$this->plugin_id . $this->id . '_merchant_id'] = $body['merchantId'];
                $merchantId = $body['merchantId'];
            }

            $this->set_post_data( $post_data );
            parent::process_admin_options();

            // Update options with merchant id
            if (isset($merchantId)) {
                $options = get_option('woocommerce_discount-payment-moona_settings');
                $options['merchant_id'] = $body['merchantId'];
                update_option('woocommerce_discount-payment-moona_settings', $options);
            }

            // Save the Cleever plugin status
            $status = ( $enabled !== '1' ) ? 'disabled' : 'enabled';
            $this->save_status($status);
        }

        /**
         * True if one key field is empty
         */
        private function is_empty_keys() 
        {
            return empty($this->moona_sk_live) || empty($this->moona_pk_live) || empty($this->moona_sk_test) || empty($this->moona_pk_test);
        }

        /**
         * Checking if this gateway is enabled.
         */
        public function is_available()
        {
            if ( $this->enabled !== 'yes' ) {
                return false;
            }
            if ( !in_array( get_woocommerce_currency(), array( 'GBP' ) ) ) {
                return false;
            }
            if ( $this->is_empty_keys() ) {
                return false;
            }
            return true;
        }

        /**
         * Return whether or not this gateway still requires setup to function.
         *
         * When this gateway is toggled on via AJAX, if this returns true a
         * redirect will occur to the settings page instead.
         *
         * @since 3.4.0
         * @return bool
         */
        public function needs_setup()
        {
            if ( $this->is_empty_keys() ) {
                return true;
            }
        }

        /**
         * Init the settings form
         */
        public function init_form_fields()
        {
            $this->form_fields = array(
                array(
                    'title'     => __( 'General settings', 'discount-payment-moona' ),
                    'type'      => 'title',
                    'id'        => 'moona_credentials_section',
                    'class'     => 'wc-title-section',
                ),
                    'enabled_main'    => array(
                        'title'       => __( 'Enable/Disable', 'discount-payment-moona' ),
                        'type'        => 'checkbox',
                        'label'       => __( 'Enable secure payment', 'discount-payment-moona' ),
                        'description' => __( 'Check the box to accept payments with credit and debit cards on your website.', 'discount-payment-moona' ),
                        'default'     => 'yes',
                    ),
                    'enabled'         => array(
                        'title'       => __( 'Enable/Disable', 'discount-payment-moona' ),
                        'type'        => 'checkbox',
                        'label'       => __( 'Enable secure payment with discounts', 'discount-payment-moona' ),
                        'description' => __( 'Check the box to activate the secure payment with discounts on your website.', 'discount-payment-moona' ),
                        'default'     => 'yes',
                    ),
                    'test_mode'        => array(
                        'title'       => __( 'Enable Test Mode', 'discount-payment-moona' ),
                        'type'        => 'checkbox',
                        'label'       => __( 'Use in TEST Mode', 'discount-payment-moona' ),
                        'description' => __( 'Use in test mode to test your setup.', 'discount-payment-moona' ),
                        'default'     => 'no',
                    ),
                    'moona_pk_live'    => array(
                        'title'       => __( 'Live publishable key', 'discount-payment-moona' ),
                        'type'        => 'text',
                        'description' => __( 'The key are in your merchant dashboard.', 'discount-payment-moona' ),
                        'desc_tip'    => true,
                        'default'     => '',
                    ),
                    'moona_sk_live'    => array(
                        'title'       => __( 'Live secret key', 'discount-payment-moona' ),
                        'type'        => 'password',
                        'desc_tip' => __( 'The key are in your merchant dashboard.', 'discount-payment-moona' ),
                        'description' => __( 'NB: You can find your keys in your <a href="https://partners.cleever.com/" target="_blank">merchant dashboard</a>. Connect to the portal > Menu > Access Information', 'discount-payment-moona' ),
                        'default'     => '',
                    ),
                    'moona_pk_test'    => array(
                        'title'       => __( 'Test publishable key', 'discount-payment-moona' ),
                        'type'        => 'text',
                        'desc_tip' => __( 'The key are in your merchant dashboard.', 'discount-payment-moona' ),
                        'default'     => '',
                    ),
                    'moona_sk_test'    => array(
                        'title'       => __( 'Test secret key', 'discount-payment-moona' ),
                        'type'        => 'password',
                        'desc_tip' => __( 'The key are in your merchant dashboard.', 'discount-payment-moona' ),
                        'default'     => '',
                    ),
                array(
                    'title'     => __( 'Product slot', 'discount-payment-moona' ),
                        'type'      => 'title',
                        'id'        => 'moona_product_slot_section',
                        'class'     => 'wc-title-section',
                    ),
                    'product_slot_enabled' => array(
                        'title'         => __( 'Enable/Disable', 'discount-payment-moona' ),
                        'type'          => 'checkbox',
                        'label'         => __( 'Enable product slot', 'discount-payment-moona' ),
                        'description'   => __( 'This activates the product page slot. Check “Enable product slot” to display the checkbox on product pages and improve your add to cart rate.', 'discount-payment-moona' ),
                        'default'       => 'no',
                    ),
                array(
                    'title'     => __( 'Shipping slot', 'discount-payment-moona' ),
                        'type'      => 'title',
                        'id'        => 'moona_shipping_slot_section',
                        'class'     => 'wc-title-section',
                    ),
                    'shipping_slot_enabled' => array(
                        'title'         => __( 'Enable/Disable', 'discount-payment-moona' ),
                        'type'          => 'checkbox',
                        'label'         => __( 'Enable shipping slot', 'discount-payment-moona' ),
                        'description'   => __( 'This activates the shipping slot. Choose below the shipping method which will be associated to the free shipping service in the shipping slot.', 'discount-payment-moona' ),
                        'default'       => 'no',
                    ),
                    'shipping_slot_associated_method' => array(
                        'title'         => __( 'Associated to delivery method', 'discount-payment-moona' ),
                        'type'          => 'select',
                        'options'       => $this->populate_shipping_slot_associated_method_dropdown(),
                    ),
                    'shipping_slot_display_associated_method' => array(
                        'title'         => __( 'Display the associated delivery method', 'discount-payment-moona' ),
                        'type'          => 'checkbox',
                        'default'       => 'no',
                    ),
                    'shipping_slot_label' => array(
                        'title'         => __( 'Label', 'discount-payment-moona' ),
                        'type'          => 'text',
                        'default'       => '',
                    ),
                    'shipping_slot_sort' => array(
                        'title'         => __( 'Sort order', 'discount-payment-moona' ),
                        'type'          => 'number',
                    ),
                array(
                    'title'     => __( 'Banners settings', 'discount-payment-moona' ),
                    'type'      => 'title',
                    'id'        => 'moona_banners_section',
                    'class'     => 'wc-title-section',
                ),
                    array(
                        'title'     => __( 'Banners at the top', 'discount-payment-moona' ),
                        // 'description'   => __( 'Define where to display a top banner.', 'discount-payment-moona' ),
                        'type'      => 'title',
                        'id'        => 'moona_top_banners_section',
                        'class'     => 'wc-subtitle-section',
                    ),
                        'banner_top_shop_archives' => array(
                            'title'         => __( ' - Shop archive pages', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount on shop archives', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                        'banner_top_product_page' => array(
                            'title'         => __( ' - Product page', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise at the top of every product page', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                        'banner_top_cart_page' => array(
                            'title'         => __( ' - Cart page', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount at the top of cart page', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                        'banner_top_checkout_page' => array(
                            'title'         => __( ' - Checkout page', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount at the top of checkout page', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                        'banner_top_thankyou_page' => array(
                            'title'         => __( ' - Order confirmation page', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount at the top of order confirmation page', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                    array(
                        'title'     => __( 'Banners above CTAs', 'discount-payment-moona' ),
                        'type'      => 'title',
                        'id'        => 'moona_banner_above_cta_section',
                        'class'     => 'wc-subtitle-section',
                    ),
                        'banner_above_cta_product_page' => array(
                            'title'         => __( ' - Product page', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount above "Add to cart" button', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                        'banner_above_cta_cart_page' => array(
                            'title'         => __( ' - Cart page', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount above "Proceed to checkout" button', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                        'banner_above_cta_checkout_page' => array(
                            'title'         => __( ' - Checkout page', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount before payment selection list', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                        'banner_above_cta_minicart' => array(
                            'title'         => __( ' - Mini cart', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount under the mini cart content list', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        ),
                    array(
                        'title'     => __( 'Square banner', 'discount-payment-moona' ),
                        'type'      => 'title',
                        'id'        => 'moona_square_banner_section',
                        'class'     => 'wc-subtitle-section',
                    ),
                        'banner_square_cart_page' => array(
                            'title'         => __( ' - Mini cart', 'discount-payment-moona' ),
                            'type'          => 'checkbox',
                            'label'         => __( 'Enable', 'discount-payment-moona' ),
                            'description'   => __( 'Advertise the discount below the mini cart', 'discount-payment-moona' ),
                            'default'       => 'yes',
                        )
            );
        }

        /**
         * Process the payment : when we click on the CTA to pay
         */
        public function process_payment( $order_id ) 
        {
            $response = $this->get_moona_session( $order_id );

            if ( is_wp_error( $response ) ) {
                $this->log( 'error on process_payment() process : get_moona_session result error' . print_r($response, true) );
                // wc_add_notice(  __( 'Error to contact the payment platform. Please try again later.', 'discount-payment-moona'), 'error' );
                return;
            }

            if ( !isset($response) || empty($response) || !array_key_exists('url', $response) ) {
                $this->log( 'error on process_payment() process : URL not found' );
                return;
            }

            $url_to_redirect = $response['url'];
            $mode = $response['redirectionMode']; // iframe or redirection
            //$display = $response['display']; // A, B or C

            if ( (filter_var($url_to_redirect, FILTER_VALIDATE_URL) === false) || (strpos($url_to_redirect, 'sessionid=') === false) ) {
                $this->log( 'error on process_payment() process : URL not valid' );
                return;
            }

            $moona_session_id = explode('sessionid=', $url_to_redirect)[1];
            update_post_meta( $order_id, '_moona_id', $moona_session_id);
            $order = wc_get_order( $order_id );

            // Some notes to customer (replace true with false to make it private)
			$order->add_order_note( __( 'Checkout started Cleever discount session ID : ', 'discount-payment-moona' ) . $moona_session_id, false );
            // $order->set_created_via('moona-discount');
            $order->save();

            // TMP
            // $url_to_redirect = str_replace('https', 'http', $url_to_redirect);
            // $url_to_redirect = str_replace('staging.secure-payment-ecommerce.com', 'localhost:3000', $url_to_redirect);

            if ($mode === 'redirection') {
                // Redirect to the Cleever gateway
                return array(
                    'result'    => 'success',
                    'redirect'  =>  $url_to_redirect
                );
            }
            // iframe
            else {
                update_post_meta( $order_id, '_url_to_redirect', $url_to_redirect);
                
                return array(
                    'result'    => 'success',
                    'redirect'  => $order->get_checkout_payment_url(true)
                );
            }
        }

        /**
         * Create the Cleever session
         */
        private function get_moona_session( $order_id ) 
        {
            // we need it to get any order details
            $order = wc_get_order( $order_id );

            // Send amplitude
            // $passerelles = WC()->payment_gateways()->get_available_payment_gateways();
            // Récupération de la position d'une passerelle
            // $key_position = array_search( 'discount-payment-moona', array_keys( WC()->payment_gateways()->get_available_payment_gateways() ) );
            // $GLOBALS['wc_moona_amplitude']->logEvent('XXX', 'YYY');

            // Init session if needed
            if( !WC()->session ) {
                WC()->session = new WC_Session_Handler();
                WC()->session->init();
            }
            
            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'amount' => round( number_format($order->get_total(), 2 , '.' , '' ) * 100),
                'currency' => get_woocommerce_currency(), // $order->get_currency() or $order_data['currency']
                'email' => $order->get_billing_email(), // $order_data['billing']['email']
                'firstname' => $order->get_billing_first_name(), // $order_data['billing']['first_name']
                'lastname' => $order->get_billing_last_name(), // $order_data['billing']['last_name']
                'phone' => $order->get_billing_phone(), // $order_data['billing']['phone']
                'address' => $order->get_billing_address_1(), // $order_data['billing']['address_1']
                'postcode' => $order->get_billing_postcode(), // $order_data['billing']['postcode']
                'city' => $order->get_billing_city(), // $order_data['billing']['city']
                'country' => $order->get_billing_country(), // // $order_data['billing']['country']
                'language' => get_locale(),
                'return_url' => $this->get_return_url( $order ),
                'cancel_url' => $this->cancel_url,
                'notification_url' => $this->notify_url,
                'order_id' => $order_id,
                'device_id' => WC()->session->get_session_cookie()[3]
            );

            // Build the payload
            $wc_moona = WC_Moona::get_instance();
            if ( $wc_moona->is_returning_user() ) {
                $shopper_infos = $wc_moona->get_shopper_infos();
                if ( ! array_key_exists( 'payload', $body ) ) {
                    $body['payload'] = array();
                }
                $body['payload']['user_infos'] = $shopper_infos;
            }

            if ( $wc_moona->is_mship_selected_in_order( $order_id ) ) {
                if ( ! array_key_exists( 'payload', $body ) ) {
                    $body['payload'] = array();
                }
                $body['payload']['slot_shipping'] = $wc_moona->mship_data_for_moona_session( $order_id );
            }

            if ( array_key_exists( 'payload', $body ) ) {
                $body['payload'] = json_encode($body['payload']);
            }

            $url = (( $this->test_mode === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT);
            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->test_mode === 'yes' ) ? $this->moona_sk_test : $this->moona_sk_live
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                $this->log( 'error on get_moona_session() process : ' . print_r($response, true) );
                return $response;
            } else {
                $body = json_decode( $response['body'], true );
                return $body;
            }
        }

        /**
         * Handle hook with $_POST parameters received 
         * Handles the callbacks received from the Cleever payment backend. 
         * Give this url to your payment processing company as the ipn response URL: 
         * USAGE:       http://website.ecommerce.com/?wc-api=WC_Moona_Gateway
         * USAGE 2.0+ : http://website.ecommerce.com/wc-api/WC_Moona_Gateway/ 
         */
        public function check_ipn_response() 
        {
            $input = file_get_contents( 'php://input' );

            //$_POST
            // For payment
            // header x-api-key
            // { "session" : {
                // 'api_version',
                // 'plugin_version',
                // 'order_id', // cart_id
                // 'transaction_id',
                // 'customer',
                // 'transfer_group'

                // 'ecommerce_session_id'
                // 'discount_amount'
                // 'amount'
                // 'moona_user_id'
            // }}

            // For refund
            // header x-api-key
            // { "session" : {
                // 'api_version',
                // 'plugin_version',
                // 'order_id',
                // 'amount' => in cents
                // 'type' => 'refund'
                // 'refund_id',
            // }}

            $post = json_decode($input, true);

            if ($post === null) {
                $this->log( 'error on check_ipn_response() process with request body empty are not valid JSON' );
            }

            $session = $post['session'];
            try {
                $order_id = sanitize_key($session['order_id']);
            } catch ( Exception $e ) {
                $this->log( 'error on check_ipn_response() process : ' . wc_print_r( $e->getErrorObject(), true ) );
                exit;
            }

            $headers = getallheaders();
            if (!isset($headers['X-Api-Key']) || ($headers['X-Api-Key'] !== $this->moona_sk_test && $headers['X-Api-Key'] !== $this->moona_sk_live)) {
                $this->log( 'error on check_ipn_response() process with authorisation : Not authorized ('.$headers['X-Api-Key'].')' );
                exit;
            }

            $order = wc_get_order( $order_id );

            // MANAGE REFUND FROM THE MOONA DASHBOARD
            if ( isset($session['type']) && $session['type'] === 'refund' ) {

                try {
                    $amount = $session['amount'] / 100;

                    $remaining_refund_amount = $order->get_remaining_refund_amount();
                    if ( $amount < 0 || $amount > $remaining_refund_amount ) {
                        $this->log( 'error on check_ipn_response() process for refund (Amount error): '. print_r($amount, true) );
                        exit;
                    }

                    $msg = __( 'Order partially refunded from Cleever dashboard.', 'discount-payment-moona' );
                    if ($remaining_refund_amount == $amount) {
                        $msg = __( 'Order fully refunded from Cleever dashboard.', 'discount-payment-moona' );
                    }

                    wc_create_refund(
                        array(
                            'amount'   => $amount,
                            'reason'     => $msg,
                            'order_id'   => $order_id,
                            'line_items' => array(),
                            'refund_payment' => false
                        )
                    );

                    $amount_formatted = wc_price( $amount, array('currency' => $order->get_currency()));
                    $order->add_order_note( sprintf( __( 'Refund processed from the Cleever dashboard. Amount : %1$s (%2$s)', 'discount-payment-moona'), $amount_formatted, $session['refund_id']) );
                } catch ( Exception $e ) {
                    $this->log( 'Refund IPN error : ' . wc_print_r( $e->getErrorObject(), true ) );
                }

            } else {
                // If the order is already completed
                if ( $order->get_status() == 'completed' ) {
                    exit;
                }
                // Trash status
                else if ( $order->get_status() == 'trash' ) {
                    $order->update_status( 'pending', true );
                }

                try {
                    $order_amount = round( number_format($order->get_total(), 2 , '.' , '' ) * 100);
                    $total_amount = intval($session['discount_amount']) + intval($session['amount']);
                    if ($order_amount != $total_amount) {
                        $order->add_order_note( sprintf( __( 'Fraud : Amounts don\'t match (order amount %1$s != payment amount %2$s)', 'discount-payment-moona'), $order->get_total(), $total_amount / 100 ));
                        exit;
                    }
                } catch ( Exception $e ) {
                    $this->log( 'error on check amounts : ' . wc_print_r( $e->getErrorObject(), true ) );
                }

                // Add note
                $order->add_order_note( __( 'IPN ok | validated by Cleever discount', 'discount-payment-moona') );
                /* translators: 1: api version 2: plugin version 3: transaction id 4: customer id */
                $order->add_order_note( sprintf( __( 'API : %1$s | PLUGIN : %2$s | transaction : %3$s | customer : %4$s | moona id : %5$s', 'discount-payment-moona'), 
                                                $session['api_version'], $session['plugin_version'], $session['transaction_id'], $session['customer'], $session['moona_user_id']) );
                /* translators: %s: transfer group */
                $order->add_order_note( sprintf( __( 'Transfer group : %s', 'discount-payment-moona'), $session['transfer_group']) );
                update_post_meta( $order_id, '_transfer_group', $session['transfer_group']);

                $order->add_order_note( sprintf( __( 'Cleever discount applied : %s', 'discount-payment-moona'), $session['discount_amount'] / 100 ) );
                update_post_meta( $order_id, '_has_moona_discount', $session['discount_amount'] > 0 ? 'true' : 'false');
                update_post_meta( $order_id, '_moona_discount_amount', $session['discount_amount']);

                update_post_meta( $order_id, '_moona_user_id', $session['moona_user_id']);
                update_post_meta( $order_id, '_transaction_id', $session['transaction_id']);

                $amount_formatted = wc_price( ($session['amount'] / 100), array('currency' => $order->get_currency()));
                $order->add_order_note( sprintf( __( 'Payment completed : %1$s (amount => %2$s)', 'discount-payment-moona'),  $session['transaction_id'], $amount_formatted ) );
                
                // we received the payment
                // $order->set_payment_method_title('Cleever discount');
                $order->payment_complete( $session['transaction_id'] );
            }
        }

        /**
         * x-api-key => Optional => The merchant private sk => Only on enabled - disabled
         * Status is mandatory => can be installed - enabled - disabled - uninstalled
         */
        private function save_status($status) {

            // Can't use get_available_payment_gateways because it's not updated at this time
            $gateways = WC()->payment_gateways->payment_gateways();
            $moonaDiscountPosition = -1;
            $moonaPaymentPosition = -1;
            $counter = 0;
            if( $gateways ) {
                foreach( $gateways as $gateway ) {
                    if ($gateway->enabled == 'yes') {
                        if ( $gateway->id === 'discount-payment-moona' ) {
                            $moonaDiscountPosition = $counter;
                        }
                        else if( $gateway->id === 'discount-payment-main-moona' ) {
                            $moonaPaymentPosition = $counter;
                        }
                        $counter += 1;
                    }
                }
            }

            $moona_options = get_option('woocommerce_discount-payment-moona_settings');

            // Don't save the plugin if there is no keys for the enabled or disabled status
            if ($status === 'enabled' || $status === 'disabled') {
                if (!$moona_options['moona_sk_live'] || !$moona_options['moona_sk_test']) {
                    return;
                }
            }

            $shipping_enabled = $moona_options['shipping_slot_enabled'] === 'yes' ? true : false;
            if ( $shipping_enabled ) {
                $shipping_instance_id = $moona_options['shipping_slot_associated_method'];
                $zones = WC_Shipping_Zones::get_zones();
                $shipping_methods = array_column( $zones, 'shipping_methods' );
                foreach ( $shipping_methods[0] as $key => $class ) {
                    if ( $class->instance_id == $shipping_instance_id ) {
                        $moona_options['shipping_slot_associated_method_label'] = $class->title;
                        break;
                    }
                }
            }

            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'status' => $status,
                'options' => json_encode($moona_options),
                'payment_position' => $moonaDiscountPosition,
                'payment_position_main' => $moonaPaymentPosition,
                'device_id' => wp_get_session_token(),

                // If there is no x-api-key
                // 'merchant_email' => (get_option('admin_email')) ? get_option('admin_email') : '',
                // 'merchant_fullname' => (get_option('blogname')) ? get_option('blogname') : '',
                // 'merchant_website' => (get_site_url()) ? get_site_url() : '', // get_option('home') or get_option('siteurl')
            );

            if ( isset( $moona_options['merchant_id'] ) ) {
                $body['merchant_id'] = $moona_options['merchant_id'];
            }

            // NOTE : CHANGE BY PROD ON RELEASE
            // STAGING
            // $url = MOONA_API_STAGING_ENDPOINT;
            // PROD
            $url = MOONA_API_ENDPOINT;

            $url .= 'ecommerce/status';

            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        // NOTE : CHANGE BY PROD ON RELEASE
                        // STAGING
                        // 'x-api-key' => $moona_options['moona_sk_test']
                        // PROD
                        'x-api-key' => $moona_options['moona_sk_live']
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                $this->log( 'error on save_status() process : ' . print_r($response, true) );
                return $response;
            } else {
                $body = json_decode( $response['body'], true );
                if ( array_key_exists ( 'merchant_id', $body ) && $body['merchant_id'] != $moona_options['merchant_id'] ) {
                    $moona_options['merchant_id'] = $body['merchant_id'];
                    update_option( 'woocommerce_discount-payment-moona_settings', $moona_options );
                }
                return $body;
            }
        }

        /**
         * Refund
         */
        public function process_refund($order_id, $amount = null, $reason = '') {

            if (function_exists('wc_get_order')) {
				$order = wc_get_order( $order_id );
			} else {
				$order = new WC_Order( $order_id );
			}

            $payment_id = $order->get_transaction_id();
            $amount_formatted = (isset($amount)) ? wc_price($amount, array('currency' => $order->get_currency())) : '(no value)';

            if (empty($payment_id)){
                $order->add_order_note(
                    sprintf(
                        __( "Failed to send refund of %s to Cleever payment (no charge ID associated).", 'discount-payment-moona' ),
                        $amount_formatted
                    )
                );
                return false;
            }
            
            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'amount' =>  (isset($amount)) ? round( number_format($amount, 2 , '.' , '' ) * 100) : null, // round ()
                'transaction_id' => $payment_id,
                'order_id' => $order_id,
                'reason' => $reason,
                'moona_discount_amount' => (empty($order->get_meta('_moona_discount_amount')) && $order->get_meta('_moona_discount_amount') !== 0 && $order->get_meta('_moona_discount_amount') !== '0') ? null : intval($order->get_meta('_moona_discount_amount')) // optional
            );
       
            $url = ( $this->test_mode === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT;
            $url .= 'ecommerce/refund';
            $response = wp_remote_post( $url, array(
                'method' => 'POST',
                'timeout' => 20,
                'headers' => array(
                    'Content-Type' => 'application/json; charset=utf-8',
                    'x-api-key' => ( $this->test_mode === 'yes' ) ? $this->moona_sk_test : $this->moona_sk_live
                ),
                'body' => json_encode($body)
            ));

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                if (is_wp_error( $response )) {
                    // foreach ($response->errors as $code => $messages_arr) {}
                    self::log( 'error on process_refund() process (Cleever discount) with WP : ' . print_r($response, true) );
                } else {
                    $error = json_decode( $response['body'], true );
                    if ($error["error"]["code"] === 'ERROR-REFUND-TRANSACTIONS-005' &&
                        $error["error"]["message"] === 'Refund already done') {
                        return true;
                    }
                    self::log( 'error on process_refund() process (Cleever discount) : ' . $error["error"]["code"] );
                    $order->add_order_note( __( "Failed to send refund of {$amount_formatted} to Cleever discount ({$payment_id} => ".$error["error"]["code"]." : ".$error["error"]["message"].").", 'discount-payment-moona' ) );
                }
                return false;
            }

            $msg = __( "Refunded : {$amount_formatted} - Payment ID : {$payment_id} - Reason: {$reason}", 'discount-payment-moona' );
            $body = json_decode( $response['body'], true );
            $msg .= (isset($body['refundId'])) ? __( " - Refund ID: ".$body['refundId']."", 'discount-payment-moona') : '';
            $msg .= (isset($body['refundMoonaId'])) ? __( " - Refund Cleever ID: ".$body['refundMoonaId']."", 'discount-payment-moona') : '';
            $order->add_order_note( $msg );

            // Only for Cleever discount full refund
            $transfer = $order->get_meta('_transfer_group');
            if ( isset($body['isFullRefund']) && $body['isFullRefund'] === true && 
            ( isset($body['refundMoonaId']) || (!empty($transfer) && strncmp($transfer, "group_", 6) !== 0) ) 
            ) {
                $order->add_order_note( 'Full refund processed' );
                // $order->update_status('refunded', '', true);
                // OR
                $max_refund = wc_format_decimal( $order->get_total() - $order->get_total_refunded() );
                if ( ! $max_refund ) { 
                    return true; 
                }
                wc_create_refund(
                    array(
                        'amount'     => $max_refund,
                        'reason'     => __( 'Order fully refunded.', 'discount-payment-moona' ),
                        'order_id'   => $order_id,
                        'line_items' => array(),
                        'refund_payment' => false
                    )
                );
            }
            
			return true;
        }

        /**
         * Logging method.
         *
         * @param string $message Log message.
         * @param string $level   Optional. Default 'alert'.
         *     emergency|alert|critical|error|warning|notice|info|debug
         */
        public static function log( $message, $level = 'alert' )
        {
            if ( ! class_exists( 'WC_Logger' ) ) {
                return;
            }
            
            if ( empty(self::$log) ) {
                self::$log = wc_get_logger();
            }
            self::$log->log( $level, $message, array(
                'source' => 'moona',
            ) );
        }

        /**
         * Return an array of available shipping methods
         * Array key = shipping method instance ID
         * Array value = shipping method title
         * This kind of array can be used in WordPress Settings API's dropdown
         */
        private function populate_shipping_slot_associated_method_dropdown() {

            $data = array();

            $zones = WC_Shipping_Zones::get_zones();
            $default_zone = new WC_Shipping_Zone(0);
            $zones[$default_zone->get_id()] = $default_zone->get_data();
            $zones[$default_zone->get_id()]['shipping_methods'] = $default_zone->get_shipping_methods();
            foreach ( $zones as $zone ) {
                foreach ( $zone['shipping_methods'] as $shipping_method ) {
                    $data[$shipping_method->instance_id] = $shipping_method->title . ' (Zone - ' . $zone['zone_name'] . ')';
                }
            }
 
            return $data;

        }
    }
}