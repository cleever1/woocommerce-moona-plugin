<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Cleever Main Payment Gateway
*/
if ( ! class_exists( 'WC_Moona_Main_Gateway' ) ) {
    class WC_Moona_Main_Gateway extends WC_Payment_Gateway 
    {
        public static $log = false ;

        /**
         * Constructor for the gateway.
         */
        public function __construct()
        {   
            $this->id = 'discount-payment-main-moona';
            $this->icon = WCMOONA_BASE_URL . 'assets/images/checkout-secure-card-logos.svg';
            $this->title = __('Credit/Debit Card', 'discount-payment-moona');
            $this->description = ' ';
            $this->method_title = __( 'Cleever - Secure payment', 'discount-payment-moona' );
            $this->method_description = __( 'Use to simply and securely process card payments on your WooCommerce website.', 'discount-payment-moona' );
            $this->has_fields = false; // Can be set to true if you create a direct payment gateway that will have fields, such as credit card fields.

            // gateways can support subscriptions, refunds, saved payment methods and simple payments like here
            $this->supports = array( 'products', 'refunds' );

            // Define our settings that are then loaded with init_settings()
            // $this->init_form_fields();

            // After init_settings() is called, we can load the settings
            $this->init_settings();
            $this->moona_options = get_option('woocommerce_discount-payment-moona_settings');
            $this->cancel_url = wc_get_cart_url();

            if ($this->moona_options === false) {
                $this->moona_options = [];
            }

            $this->enabled = (isset($this->moona_options['enabled'])) ? $this->moona_options['enabled'] : 'no';            
            $this->enabled_main = (isset($this->moona_options['enabled_main'])) ? $this->moona_options['enabled_main'] : 'no';
            $this->test_mode = (isset($this->moona_options['test_mode'])) ? $this->moona_options['test_mode'] : 'yes';
            $this->moona_pk_test = (isset($this->moona_options['moona_pk_test'])) ? $this->moona_options['moona_pk_test'] : null;
            $this->moona_sk_test = (isset($this->moona_options['moona_sk_test'])) ? $this->moona_options['moona_sk_test'] : null;
            $this->moona_pk_live = (isset($this->moona_options['moona_pk_live'])) ? $this->moona_options['moona_pk_live'] : null;
            $this->moona_sk_live = (isset($this->moona_options['moona_sk_live'])) ? $this->moona_options['moona_sk_live'] : null;
            $this->merchant_id = (isset($this->moona_options['merchant_id'])) ? $this->moona_options['merchant_id'] : null;

            // Refresh title and description
            $this->title = (!empty($this->moona_options['_moona_payment_checkout_title'])) ? $this->moona_options['_moona_payment_checkout_title'] : $this->title;
            $this->icon = (!empty($this->moona_options['_moona_payment_checkout_icon'])) ? $this->moona_options['_moona_payment_checkout_icon'] : $this->icon;
            $this->description = (!empty($this->moona_options['_moona_payment_checkout_description'])) ? $this->moona_options['_moona_payment_checkout_description'] : $this->description;
            
            // Sets the callback/IPN URL
            $this->notify_url = add_query_arg( 'wc-api', 'WC_Moona_Main_Gateway', home_url( '/' ) );

            // Set the hook to send payment completed information
            // http://website.com/wc-api/{$this->webhook_name}/
            $this->webhook_name = 'wc_moona_main_gateway';
            add_action( 'woocommerce_api_' . $this->webhook_name, array( $this, 'check_ipn_main_response' ) );

            // Custom title here
            if ( $this->is_available() && is_checkout() ) {
                $this->customize_checkout_main_infos();
            }

            add_action( 'woocommerce_receipt_'. $this->id, array( $this, 'display_iframe_on_checkout_main' ) );
            
            // Do action at the end for other plugin
            do_action( 'wc_moona_main.loaded' );
        }

        /**
         * Display the iframe
         */
        public function display_iframe_on_checkout_main($order_id) {

            $order = wc_get_order($order_id);

            // IFRAME
            // Allow to go back when the payment is finished
            ?>
            <script>
                jQuery(function($) {
                    window.addEventListener("message", (event) => {
                        if (!event.origin.includes("secure-payment-ecommerce.com") && !event.origin.includes("secure-payment.moona.com") && !event.origin.includes("e-securepay.com"))
                            return;

                        var data = event.data;
                        if (data.source !== "Moona" && data.source !== "Cleever") {
                            // console.log("Cleever payment: Wrong source");
                            return;
                        } else {
                            // console.log("Cleever PAYMENT");

                            switch (data.action) {
                                case "open-new-tab":
                                    window.open(data.url, "_blank");
                                    break;
                                case "redirect":
                                    window.top.history.replaceState("", "", data.url);
                                    window.top.location.reload();
                                    break;
                                case "finish-moona-process":
                                    window.top.history.replaceState("", "", data.url);
                                    window.top.location.reload();
                                    break;
                                case "go-to-cart":
                                    window.top.history.replaceState("", "", "<?php echo wc_get_cart_url(); ?>");
                                    window.top.location.reload();
                                    break;
                                case "go-to-checkout":
                                    window.top.history.replaceState("", "", "<?php echo wc_get_checkout_url(); ?>");
                                    window.top.location.reload();
                                    break;
                                case "cancel":
                                    window.top.history.replaceState("", "", "<?php echo wc_get_checkout_url(); ?>");
                                    window.top.location.reload();
                                    break;
                                case "paid":
                                    // { has_moona_discount: true, order_id: XXX }
                                    let json = {
                                        action: 'amplitude_action',
                                        step: 'order_paid',
                                        order_id: data.payload.order_id,
                                        has_moona_discount: data.payload.has_moona_discount
                                    };
                                    $.post(ajax_object.ajax_url, json);
                                    break;
                                case "payment:error":
                                    break;
                                case "popin-offer:full-price":
                                    // {order_id: '328'}
                                    break;
                                case "popin-offer:discount":
                                    // $element = $('span.woocommerce-Price-amount');
                                    // $element.css('text-decoration', 'line-through');
                                    // $element.after('<span class="woocommerce-Price-amount amount" style="color:red; font-weight: 600;">' + data.payload.total + '</span>');
                                    let elements = document.getElementsByClassName('order_details');
                                    elements = elements[0].getElementsByClassName('woocommerce-Price-amount amount');
                                    let requiredElement = elements[0];
                                    requiredElement.style.textDecoration = 'line-through';
                                    let htmlToAdd = '<span class="woocommerce-Price-amount amount" style="color:red; font-weight: 600;"> ' + data.payload.total + '</span>';
                                    requiredElement.insertAdjacentHTML('afterend', htmlToAdd);
                                    break;
                                default:
                                    console.log("There is no response of Cleever payment API");
                            }
                        }
                    }, false);
                });  
            </script>

            <div class="payment-popin-container" >
                <div>
                    <iframe
                        id="payment-form-iframe"
                        name="payment-iframe"
                        src="<?php echo $order->get_meta('_url_to_redirect'); ?>"
                        scrolling="yes"
                        frameborder="0"
                        border="0"
                        height="100%"
                        width="100%"
                    >
                    </iframe>
                </div>
            </div>
            <?php
        }

        /**
         * Customize the checkout information according to WS
         */
        private function customize_checkout_main_infos() {

            // Update title and icon every 24h
            $options = get_option('woocommerce_discount-payment-moona_settings');
            
            if ( isset($options['_timestamp_moona_payment_checkout']) ) {
                if ( ($options['_timestamp_moona_payment_checkout'] + 24*60*60) > time() ) {
                    return;
                }
            }
            $options['_timestamp_moona_payment_checkout'] = time();
            
            $locale = get_locale();
            $total = WC()->cart->total;

            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'lang' => $locale, // en_GB
                'price' => $total // 54.50
            );

            if (isset($options['merchant_id'])) {
                $body['merchant_id'] = $options['merchant_id'];
            }

            $url = (( $this->test_mode === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT);
            $url .= 'ecommerce/checkout_main';

            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->test_mode === 'yes' ) ? $this->moona_sk_test : $this->moona_sk_live
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                $this->log( 'error on customize_checkout_main_infos() process : ' . print_r($response, true) );
            } else {
                $body = json_decode( $response['body'], true );
                $this->title = $body['title'];
                $this->icon = $body['icon'];
                $this->description= $body['description'];

                $options['_moona_payment_checkout_title'] = $this->title;
                $options['_moona_payment_checkout_icon'] = $this->icon;
                $options['_moona_payment_checkout_description'] = $this->description;
                update_option('woocommerce_discount-payment-moona_settings', $options);
            }
        }

        /**
         * True if one key field is empty
         */
        private function is_empty_keys() 
        {
            return empty($this->moona_sk_live) || empty($this->moona_pk_live) || empty($this->moona_sk_test) || empty($this->moona_pk_test);
        }

        /**
         * Checking if this gateway is enabled.
         */
        public function is_available()
        {
            // Cleever payment can't be activated alone (Cleever discount has to be activated too)
            if ( $this->enabled_main !== 'yes' /*|| $this->enabled !== 'yes'*/ ) {
                return false;
            }
            if ( !in_array( get_woocommerce_currency(), array( 'GBP' ) ) ) {
                return false;
            }
            if ( $this->is_empty_keys() ) {
                return false;
            }
            return true;
        }

        /**
         * Return whether or not this gateway still requires setup to function.
         *
         * When this gateway is toggled on via AJAX, if this returns true a
         * redirect will occur to the settings page instead.
         *
         * @since 3.4.0
         * @return bool
         */
        public function needs_setup()
        {
            if ( $this->is_empty_keys() ) {
                return true;
            }
            return false;
        }

        public function process_payment( $order_id ) 
        {
            $response = $this->get_moona_main_session( $order_id, $_POST['payment_payload'] );

            if ( is_wp_error( $response ) ) {
                $this->log( 'error on process_payment() process : get_moona_main_session result error' . print_r($response, true) );
                // wc_add_notice(  __( 'Error to contact the payment platform. Please try again later.', 'discount-payment-moona'), 'error' );
                return;
            }

            if ( !isset($response) || empty($response) || !array_key_exists('url', $response) ) {
                $this->log( 'error on process_payment() process : URL not found' );
                return;
            }

            $url_to_redirect = $response['url'];

            if ( (filter_var($url_to_redirect, FILTER_VALIDATE_URL) === false) || (strpos($url_to_redirect, 'sessionid=') === false) ) {
                $this->log( 'error on process_payment() process : URL not valid' );
                return;
            }

            $moona_session_id = explode('sessionid=', $url_to_redirect)[1];
            update_post_meta( $order_id, '_moona_id', $moona_session_id);
            $order = wc_get_order( $order_id );

            // Some notes to customer (replace true with false to make it private)
			$order->add_order_note( __( 'Checkout started Cleever payment session ID : ', 'discount-payment-moona' ) . $moona_session_id, false );

            // TMP
            // $url_to_redirect = str_replace('https', 'http', $url_to_redirect);
            // $url_to_redirect = str_replace('staging.secure-payment-ecommerce.com', 'localhost:3000', $url_to_redirect);

            // Add meta
            update_post_meta( $order_id, '_url_to_redirect', $url_to_redirect);
            // $order->set_created_via('moona-payment');
            $order->save();

            return array(
                'result'    => 'success',
                'redirect'  => $order->get_checkout_payment_url(true)
            );
        }

        /**
         * Create the Cleever session
         */
        private function get_moona_main_session( $order_id, $payload ) 
        {
            // we need it to get any order details
            $order = wc_get_order( $order_id );

            // Init session if needed
            if( !WC()->session ) {
                WC()->session = new WC_Session_Handler();
                WC()->session->init();
            }

            $payload = stripslashes($payload);
            $payloadArray = json_decode($payload, true);

            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'amount' => round( number_format($order->get_total(), 2 , '.' , '' ) * 100),
                'currency' => get_woocommerce_currency(), // $order->get_currency() or $order_data['currency']
                'email' => $order->get_billing_email(), // $order_data['billing']['email']
                'firstname' => $order->get_billing_first_name(), // $order_data['billing']['first_name']
                'lastname' => $order->get_billing_last_name(), // $order_data['billing']['last_name']
                'phone' => $order->get_billing_phone(), // $order_data['billing']['phone']
                'address' => $order->get_billing_address_1(), // $order_data['billing']['address_1']
                'postcode' => $order->get_billing_postcode(), // $order_data['billing']['postcode']
                'city' => $order->get_billing_city(), // $order_data['billing']['city']
                'country' => $order->get_billing_country(), // // $order_data['billing']['country']
                'language' => get_locale(),
                'return_url' => $this->get_return_url( $order ),
                'cancel_url' => $this->cancel_url,
                'notification_url' => $this->notify_url,
                'order_id' => $order_id,
                'device_id' => WC()->session->get_session_cookie()[3],
                'mode' => 'integrated',
                'payment_method' => $payloadArray["payment_method"]
            );

            // Build the payload
            $body['payload'] = $payloadArray;

            $wc_moona = WC_Moona::get_instance();
            if ( $wc_moona->is_returning_user() ) {
                $shopper_infos = $wc_moona->get_shopper_infos();
                if ( ! array_key_exists( 'payload', $body ) ) {
                    $body['payload'] = array();
                }
                $body['payload']['user_infos'] = $shopper_infos;
            }

            if ( $wc_moona->is_mship_selected_in_order( $order_id ) ) {
                if ( ! array_key_exists( 'payload', $body ) ) {
                    $body['payload'] = array();
                }
                $body['payload']['slot_shipping'] = $wc_moona->mship_data_for_moona_session( $order_id );
            }

            if ( $this->moona_options['product_slot_enabled'] ) {
                if ( ! array_key_exists( 'payload', $body ) ) {
                    $body['payload'] = array();
                }
                if ( isset( $_COOKIE['slpcbx'] ) ) {
                    $body['payload']['slot_product_checkbox_checked'] = true;
                }
                else {
                    $body['payload']['slot_product_checkbox_checked'] = false;
                }
            }
            $body['payload'] = json_encode($body['payload']);

            $url = ( $this->test_mode === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT;
            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->test_mode === 'yes' ) ? $this->moona_sk_test : $this->moona_sk_live
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                $this->log( 'error on get_moona_main_session() process : ' . print_r($response, true) );
                return $response;
            } else {
                $body = json_decode( $response['body'], true );
                return $body;
            }
        }

        /**
         * Handle hook with $_POST parameters received 
         * Handles the callbacks received from the Cleever payment backend. 
         * Give this url to your payment processing company as the ipn response URL: 
         * USAGE:       http://website.ecommerce.com/?wc-api=WC_Moona_Gateway
         * USAGE 2.0+ : http://website.ecommerce.com/wc-api/WC_Moona_Gateway/ 
         */
        public function check_ipn_main_response() 
        {
            $input = file_get_contents( 'php://input' );

            //$_POST
            // For payment
            // header x-api-key
            // { "session" : {
                // 'api_version',
                // 'plugin_version',
                // 'order_id', // cart_id
                // 'transaction_id',
                // 'customer',
                // 'transfer_group'

                // 'ecommerce_session_id'
                // 'discount_amount'
                // 'amount'
                // 'moona_user_id'
            // }}

            // For refund
            // header x-api-key
            // { "session" : {
                // 'api_version',
                // 'plugin_version',
                // 'order_id',
                // 'amount' => in cents
                // 'type' => 'refund'
                // 'refund_id',
            // }}

            $post = json_decode($input, true);

            if ($post === null) {
                $this->log( 'error on check_ipn_main_response() process with request body empty are not valid JSON' );
            }

            $session = $post['session'];
            try {
                $order_id = sanitize_key($session['order_id']);
            } catch ( Exception $e ) {
                $this->log( 'error on check_ipn_main_response() process : ' . wc_print_r( $e->getErrorObject(), true ) );
                exit;
            }

            $headers = getallheaders();
            if (!isset($headers['X-Api-Key']) || ($headers['X-Api-Key'] !== $this->moona_sk_test && $headers['X-Api-Key'] !== $this->moona_sk_live)) {
                $this->log( 'error on check_ipn_main_response() process with authorisation : Not authorized ('.$headers['X-Api-Key'].')' );
                exit;
            }

            $order = wc_get_order( $order_id );

            // MANAGE REFUND FROM THE MOONA DASHBOARD
            if ( isset($session['type']) && $session['type'] === 'refund' ) {
                
                try {
                    $amount = $session['amount'] / 100;

                    $remaining_refund_amount = $order->get_remaining_refund_amount();
                    if ( $amount < 0 || $amount > $remaining_refund_amount ) {
                        $this->log( 'error on check_ipn_main_response() process for refund (Amount error): '. print_r($amount, true) );
                        exit;
                    }

                    $msg = __( 'Order partially refunded from Cleever dashboard.', 'discount-payment-moona' );
                    if ($remaining_refund_amount == $amount) {
                        $msg = __( 'Order fully refunded from Cleever dashboard.', 'discount-payment-moona' );
                    }

                    wc_create_refund(
                        array(
                            'amount'   => $amount,
                            'reason'     => $msg,
                            'order_id'   => $order_id,
                            'line_items' => array(),
                            'refund_payment' => false
                        )
                    );

                    $amount_formatted = wc_price( $amount, array('currency' => $order->get_currency()));
                    $order->add_order_note( sprintf( __( 'Refund processed from the Cleever dashboard. Amount : %1$s (%2$s)', 'discount-payment-moona'), $amount_formatted, $session['refund_id']) );
                } catch ( Exception $e ) {
                    $this->log( 'Refund IPN error : ' . wc_print_r( $e->getErrorObject(), true ) );
                }

            } else {
                // If the order is already completed
                if ( $order->get_status() == 'completed' ) {
                    exit;
                } 
                // Trash status
                else if ( $order->get_status() == 'trash' ) {
                    $order->update_status( 'pending', true );
                }

                try {
                    $order_amount = round( number_format($order->get_total(), 2 , '.' , '' ) * 100);
                    $total_amount = intval($session['discount_amount']) + intval($session['amount']);
                    if ($order_amount != $total_amount) {
                        $order->add_order_note( sprintf( __( 'Fraud : Amounts don\'t match (order amount %1$s != payment amount %2$s)', 'discount-payment-moona'), $order->get_total(), $total_amount / 100 ));
                        exit;
                    }
                } catch ( Exception $e ) {
                    $this->log( 'error on check amounts : ' . wc_print_r( $e->getErrorObject(), true ) );
                }

                // Add note
                $order->add_order_note( __( 'IPN ok | validated by Cleever payment', 'discount-payment-moona') );
                /* translators: 1: api version 2: plugin version 3: transaction id 4: customer id */
                $order->add_order_note( sprintf( __( 'API : %1$s | PLUGIN : %2$s | transaction : %3$s | customer : %4$s | moona id : %5$s', 'discount-payment-moona'), 
                                                $session['api_version'], $session['plugin_version'], $session['transaction_id'], $session['customer'], $session['moona_user_id']) );
                /* translators: %s: transfer group */
                $order->add_order_note( sprintf( __( 'Transfer group : %s', 'discount-payment-moona'), $session['transfer_group']) );
                update_post_meta( $order_id, '_transfer_group', $session['transfer_group']);
                
                if ( strncmp($session['transfer_group'], "group_", 6) !== 0 ) {
                    $order->add_order_note( sprintf( __( 'Cleever discount applied : %s', 'discount-payment-moona'), $session['discount_amount'] / 100 ) );
                    update_post_meta( $order_id, '_has_moona_discount', 'true');
                    update_post_meta( $order_id, '_moona_discount_amount', $session['discount_amount']);
                }
                // Start by group_ (Cleever full payment)
                else {
                    update_post_meta( $order_id, '_has_moona_discount', 'false');
                    update_post_meta( $order_id, '_moona_discount_amount', 0);
                }
                update_post_meta( $order_id, '_moona_user_id', $session['moona_user_id']);
                update_post_meta( $order_id, '_transaction_id', $session['transaction_id']);

                $amount_formatted = wc_price( ($session['amount'] / 100), array('currency' => $order->get_currency()));
                $order->add_order_note( sprintf( __( 'Payment completed : %1$s (amount => %2$s)', 'discount-payment-moona'),  $session['transaction_id'], $amount_formatted ) );
                
                // we received the payment
                // $order->set_payment_method_title('Cleever payment');
                $order->payment_complete( $session['transaction_id'] );
            }
        }

        /**
         * Add integrated iframe
         */
        public function payment_fields()
        {
            $mode = ( $this->test_mode === 'yes' ) ? 'staging.' : '';

            $wc_moona = WC_Moona::get_instance();
            $shopper_infos_formatted = $wc_moona->get_shopper_infos_formatted();

            $cart_amount = WC()->cart->get_total( 'raw' ); // anything else than 'view' return amount in float
            $cart_amount = round( number_format($cart_amount, 2 , '.' , '' ) * 100);

            $slfs = '';
            if ( $wc_moona->show_mship() ) {
                $slfs_value = $wc_moona->is_mship_selected() ? 1 : 0;
                $slfs = '&slfs=' . $slfs_value;
            }

            $slpcbx = '';
            if ( $this->moona_options['product_slot_enabled'] ) {
                if ( isset( $_COOKIE['slpcbx'] ) ) {
                    $slpcbx = '&slpcbx=1';
                }
                else {
                    $slpcbx = '&slpcbx=0';
                }
            }

            $url = 'https://'.$mode.'secure-payment-ecommerce.com/card-form?cms='.WCMOONA_ECOMMERCE_SOLUTION.'&v='.WC_VERSION.'&pv='.WCMOONA_PLUGIN_VERSION.'&mid='.$this->merchant_id . $shopper_infos_formatted . '&cp=' . $cart_amount . $slfs . $slpcbx;

            if (isset($this->description) && !empty($this->description)) {
                echo '<div>'.$this->description.'</div>';
            }

            ?>
            <div id="payment-integrated-container" name="payment-integrated-container" class="payment-integrated-container" >
                <div>
                    <iframe
                        id="payment-form-integrated"
                        name="payment-integrated"
                        src="<?php echo $url; ?>"
                        scrolling="yes"
                        frameborder="0"
                        border="0"
                        height="100%"
                        width="100%">
                    </iframe>
                    <input id="payment_payload" name="payment_payload" type="hidden" />
                </div>
            </div>
            <div class="clear"></div>
            <?php
        }

        /**
         * Refund
         */
        public function process_refund($order_id, $amount = null, $reason = '') {

            if (function_exists('wc_get_order')) {
				$order = wc_get_order( $order_id );
			} else {
				$order = new WC_Order( $order_id );
			}

            $payment_id = $order->get_transaction_id();
            $amount_formatted = (isset($amount)) ? wc_price($amount, array('currency' => $order->get_currency())) : '(no value)';

            if (empty($payment_id)) {
                $transfer = $order->get_meta('_transfer_group');
                if (!empty($transfer) && strncmp($transfer, "group_", 6) === 0) {
                    $payment_id = substr($transfer, 6);
                } else {
                    $order->add_order_note(
                        sprintf(
                            __( "Failed to send refund of %s to Cleever payment (no charge ID associated).", 'discount-payment-moona' ),
                            $amount_formatted
                        )
                    );
                    return false;
                }
            }

            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'amount' =>  (isset($amount)) ? round( number_format($amount, 2 , '.' , '' ) * 100) : null, // round ()
                'transaction_id' => $payment_id,
                'order_id' => $order_id,
                'reason' => $reason,
                'moona_discount_amount' => (empty($order->get_meta('_moona_discount_amount')) && $order->get_meta('_moona_discount_amount') !== 0 && $order->get_meta('_moona_discount_amount') !== '0') ? null : intval($order->get_meta('_moona_discount_amount')) // optional
            );
       
            $url = ( $this->test_mode === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT;
            $url .= 'ecommerce/refund';
            $response = wp_remote_post( $url, array(
                'method' => 'POST',
                'timeout' => 20,
                'headers' => array(
                    'Content-Type' => 'application/json; charset=utf-8',
                    'x-api-key' => ( $this->test_mode === 'yes' ) ? $this->moona_sk_test : $this->moona_sk_live
                ),
                'body' => json_encode($body)
            ));

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                if (is_wp_error( $response )) {
                    // foreach ($response->errors as $code => $messages_arr) {}
                    self::log( 'error on process_refund() process (Cleever payment) with WP : ' . print_r($response, true) );
                } else {
                    $error = json_decode( $response['body'], true );
                    if ($error["error"]["code"] === 'ERROR-REFUND-TRANSACTIONS-005' &&
                        $error["error"]["message"] === 'Refund already done') {
                        return true;
                    }
                    self::log( 'error on process_refund() process (Cleever payment) : ' . $error["error"]["code"] );
                    $order->add_order_note( __( "Failed to send refund of {$amount_formatted} to Cleever payment ({$payment_id} => ".$error["error"]["code"]." : ".$error["error"]["message"].").", 'discount-payment-moona' ) );
                }
                return false;
            }

            $msg = __( "Refunded : {$amount_formatted} - Payment ID : {$payment_id} - Reason: {$reason}", 'discount-payment-moona' );
            $body = json_decode( $response['body'], true );
            $msg .= (isset($body['refundId'])) ? __( " - Refund ID: ".$body['refundId']."", 'discount-payment-moona') : '';
            $msg .= (isset($body['refundMoonaId'])) ? __( " - Refund Cleever ID: ".$body['refundMoonaId']."", 'discount-payment-moona') : '';
            $order->add_order_note( $msg );

            // Only for Cleever discount full refund
            $transfer = $order->get_meta('_transfer_group');
            if ( isset($body['isFullRefund']) && $body['isFullRefund'] === true && 
            ( isset($body['refundMoonaId']) || (!empty($transfer) && strncmp($transfer, "group_", 6) !== 0) ) 
            ) {
                $order->add_order_note( 'Full refund processed' );
                // $order->update_status('refunded', '', true);
                // OR
                $max_refund = wc_format_decimal( $order->get_total() - $order->get_total_refunded() );
                if ( ! $max_refund ) { 
                    return true; 
                }
                wc_create_refund(
                    array(
                        'amount'     => $max_refund,
                        'reason'     => __( 'Order fully refunded.', 'discount-payment-moona' ),
                        'order_id'   => $order_id,
                        'line_items' => array(),
                        'refund_payment' => false
                    )
                );
            }
            
			return true;
        }

        /**
         * Logging method.
         *
         * @param string $message Log message.
         * @param string $level   Optional. Default 'alert'.
         *     emergency|alert|critical|error|warning|notice|info|debug
         */
        public static function log( $message, $level = 'alert' )
        {
            if ( ! class_exists( 'WC_Logger' ) ) {
                return;
            }
            
            if ( empty(self::$log) ) {
                self::$log = wc_get_logger();
            }
            self::$log->log( $level, $message, array(
                'source' => 'moona',
            ) );
        }
    }
}