<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
    // Exit if accessed directly
}

class MoonaAmplitude {

    public static $log = false;

    private $moona_options;

    const AMPLITUDE_API_URL = 'https://api2.amplitude.com/2/httpapi';
    const AMPLITUDE_API_KEY = 'fceaa7f5144bfd64c3281bbca7cf0bac';
    const AMPLITUDE_API_KEY_STAGING = 'a19d8d2c919ec80110ac6d2353253c9e';
    
    public function __construct($apiKey = null)
    {
        $this->moona_options = get_option('woocommerce_discount-payment-moona_settings');

        // if ( is_admin() ) {
        add_action( 'wp_ajax_amplitude_action', array( $this, 'amplitude_action') );
        add_action( 'wp_ajax_nopriv_amplitude_action', array( $this, 'amplitude_action') );
        // }
    }

    function amplitude_action() {

        // Allow to force the session initialization
        do_action( 'woocommerce_set_cart_cookies',  true );
        
        // // OR En fonction de là où on est dans le code
        // // Ensure that customer session is started
        // if( !WC()->session->has_session() )
        // WC()->session->set_customer_session_cookie(true);

        if (isset($_POST['step'])) {
            $step = $_POST['step'];
            if ($step == 'change_payment_method') {

                $shortVersion = explode(".", WC_VERSION);
                $paymentKeys = array_keys( WC()->payment_gateways->get_available_payment_gateways() );
                $moonaDiscountPosition = array_search( 'discount-payment-moona',  $paymentKeys) ;
                $moonaPaymentPosition = array_search( 'discount-payment-main-moona', $paymentKeys) ;
                $moonaDiscountPosition = ($moonaDiscountPosition !== false) ? $moonaDiscountPosition : -1;
                $moonaPaymentPosition = ($moonaPaymentPosition !== false) ? $moonaPaymentPosition : -1;

                $paymentMethodID = $_POST['payment_method_id'];
                if ($paymentMethodID === 'discount-payment-moona') {
                    $paymentMethodID = 'moona-discount';
                } else if ($paymentMethodID === 'discount-payment-main-moona') {
                    $paymentMethodID = 'moona-payment';
                }
                
                $eventProperties = array(
                    'merchantSite' => get_bloginfo( 'name' ),
                    'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                    'moonaPaymentPosition' => $moonaPaymentPosition,
                    'moonaDiscountPosition' => $moonaDiscountPosition,
                    'currency' => get_woocommerce_currency(),
                    'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                    'orderTotalValue' => WC()->cart->total,
                    'paymentMethodID' => $paymentMethodID,
                    'paymentMethodLabel' => $_POST['payment_method_label'],
                    'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                    'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                    'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                    'pluginVersion' => WCMOONA_PLUGIN_VERSION
                );
    
                if (isset($this->moona_options['merchant_id'])) {
                    $eventProperties['merchantID'] = $this->moona_options['merchant_id'];
                }

                $this->logEvent('Click - Merchant checkout - Change payment method', $eventProperties);
            } 
            else if ($step == 'order_paid') {
                update_post_meta( $_POST['order_id'], '_has_moona_discount', $_POST['has_moona_discount']);
                update_post_meta( $_POST['order_id'], '_is_paid', 'true');

                $order = wc_get_order( $_POST['order_id'] );
                $order->add_order_note( __( 'Payment is successful (Awaiting for transaction id to enable refund). Order can be processed.', 'discount-payment-moona') );
                $order->update_status( 'processing', true );
            }
        }

        // WC()->session->set( 'moona_deviceid', $_POST['deviceId']);
        wp_die();
    }

    function logEvent($eventName, $eventProperties, $userId = null) {
        // Note that one of user_id or device_id is required, as well as the event_type
        // Note: '[Amplitude] Country', '[Amplitude] City', '[Amplitude] Region', and '[Amplitude] DMA' are user properties pulled using GeoIP. We use MaxMind's database, which is widely accepted as the most reliable digital mapping source, to lookup location information from the user's IP address. For any HTTP API events, if GeoIP information is unavailable, then we pull the information from the 'location_lat' and 'location_lng' keys if those keys are populated. If these location properties are manually set, then Amplitude will not modify that property.

        // time         // ISO 8601 format (YYYY-MM-DDTHH:mm:ss) => must be number of milliseconds since the start of epoch time
        // insert_id
        // event_type
        // device_id
        // user_id

        // // Event
        // 'user_id' => 'datamonster@gmail.com',
        // 'device_id' => 'C8F9E604-F01A-4BD9-95C6-8E5357DF265D',
        // 'event_type' => 'watch_tutorial',
        // 'time' => 1396381378123,
        // 'insert_id' => 'LKJLKJLJLJ',
        // 'event_properties' => array(
        //     'load_time' => 0.8371,
        //     'source' => "notification"
        // ),
        // 'user_properties' => array(
        //     'age' => 25,
        //     'gender' => 'female'
        // )

        // Init session if needed
        if( !WC()->session ) {
            WC()->session = new WC_Session_Handler();
            WC()->session->init();
        }
        
        try {
            $deviceId = WC()->session->get_session_cookie()[3];
        } catch (Exception $e) {
            $deviceId = null;
        }

        if (empty($userId) && empty($deviceId)) {
            if (isset($this->moona_options['merchant_id'])) {
                $userId = $this->moona_options['merchant_id'];
            } else {
                $deviceId = wp_generate_uuid4();
            }
        }

        // https://stackoverflow.com/questions/38546354/woocommerce-cookies-and-sessions-get-the-current-products-in-cart
        $body = array(
            'api_key' => ( $this->moona_options['test_mode'] === 'yes' ) ? static::AMPLITUDE_API_KEY_STAGING : static::AMPLITUDE_API_KEY,
            'events' => array(array(
                'user_id' => $userId,
                'device_id' => $deviceId,
                'event_type' => $eventName,
                'event_properties' => $eventProperties
            ))
        );
        
        $response = wp_remote_post(static::AMPLITUDE_API_URL, array(
            'method' => 'POST',
            'timeout' => 60,
            'redirection' => 5,
            'blocking' => true,
            'headers' => array(
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ),
            'body' => json_encode($body)
        ));

        if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
            $this->log( 'error on logEvent() process : ' . print_r($response, true) );
        }
    }

    function saveMoonaPosition() {
        if (isset($this->moona_options['merchant_id'])) {
            $userId = $this->moona_options['merchant_id'];

            $paymentKeys = array_keys( WC()->payment_gateways->get_available_payment_gateways() );
            $moonaDiscountPosition = array_search( 'discount-payment-moona',  $paymentKeys) ;
            $moonaPaymentPosition = array_search( 'discount-payment-main-moona', $paymentKeys) ;

            $userProperties = array(
                'moonaPaymentPosition' => $moonaPaymentPosition,
                'moonaDiscountPosition' => $moonaDiscountPosition,
            );

            $body = array(
                'api_key' => ( $this->moona_options['test_mode'] === 'yes' ) ? static::AMPLITUDE_API_KEY_STAGING : static::AMPLITUDE_API_KEY,
                'events' => array(array(
                    'user_id' => $userId,
                    'user_properties' => $userProperties,
                    'event_type' => 'Merchant - Update Moona position',
                ))
            );

            $response = wp_remote_post(static::AMPLITUDE_API_URL, array(
                'method' => 'POST',
                'timeout' => 60,
                'redirection' => 5,
                'blocking' => true,
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Accept' => '*/*'
                ),
                'body' => json_encode($body)
            ));
    
            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                $this->log( 'error on saveMoonaPosition() process : ' . print_r($response, true) );
            }
        }
    }

    public static function log( $message, $level = 'alert' ) {
        if ( ! class_exists( 'WC_Logger' ) ) {
            return;
        }
        if ( empty(self::$log) ) {
            self::$log = wc_get_logger();
        }
        self::$log->log( $level, $message, array(
            'source' => 'moona',
        ) );
    }
}

$GLOBALS['wc_moona_amplitude'] = new MoonaAmplitude();

?>
