<?php

/**
 * Plugin Name: Cleever payment
 * Plugin URI: https://gitlab.com/moona1/woocommerce-moona-plugin
 * Description: Cleever - WooCommerce plugin allow to offer discount to your shoppers
 * Author: Cleever
 * Author URI: https://www.cleever.com
 * Version: 2.2.0
 * Text Domain: discount-payment-moona
 * Domain Path: /languages/
 * WC requires at least: 3.2.0
 * WC tested up to: 6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
    // Exit if accessed directly
}

defined( 'WCMOONA_ECOMMERCE_SOLUTION' ) || define( 'WCMOONA_ECOMMERCE_SOLUTION', 'WooCommerce' );
defined( 'WCMOONA_PLUGIN_VERSION' ) || define( 'WCMOONA_PLUGIN_VERSION', 'WC_2.2.0' );
defined( 'WCMOONA_API_VERSION' ) || define( 'WCMOONA_API_VERSION', '1.0.0' );
defined( 'WCMOONA_BASE_PATH' ) || define( 'WCMOONA_BASE_PATH', realpath( dirname( __FILE__ ) ) );
defined( 'WCMOONA_BASE_URL' ) || define( 'WCMOONA_BASE_URL', plugin_dir_url( __FILE__ ) );
defined( 'WCMOONA_BASE_NAME' ) || define( 'WCMOONA_BASE_NAME', basename( dirname( __FILE__ ) ) ); // dirname( plugin_basename( __FILE__ ) ) . '/'
defined( 'WCMOONA_FILE_VERSION' ) || define( 'WCMOONA_FILE_VERSION', '2.2.0' );

defined( 'MOONA_API_ENDPOINT' ) || define( 'MOONA_API_ENDPOINT', 'https://api.moona.com/' );
defined( 'MOONA_API_STAGING_ENDPOINT' ) || define( 'MOONA_API_STAGING_ENDPOINT', 'https://staging.api.moona.com/' );

if ( ! class_exists( 'WC_Moona' ) ) {

    /**
     * Class WC_Moona
     */
    class WC_Moona {

        public static $log = false ;

        private $moona_options;
        private $banners = array();
        private $display_discount;
        private $shopper_infos;
        private $shipping_infos_first_load_done;
        private $call_get_shipping_slot_infos_on_update;
        private $shipping_infos_new_user;
        private $shipping_infos_returning_user;
        private $product_slot_data;

        private static $banner_html = '<div style="color: black;border: black 4px solid;padding:10px;text-align:center;margin-bottom:20px;"><div style="line-height: initial; font-weight: bold;">Get £5 OFF at checkout</div></div>';
        private static $banner_post_order_html = '<div style="color: black;border: black 4px solid;padding:10px;text-align:center;margin-bottom:20px;"><div style="line-height: initial; font-weight: bold;">Get £5 OFF at checkout</div></div>';
        private static $banner_above_cta_text_html = '<div style="display: table; color: black;border: black 4px solid;padding:10px;text-align:center;margin-bottom:20px;"><div style="line-height: initial; font-weight: bold;">Get £5 OFF at checkout</div></div>';
        private static $banner_center_above_cta_html = '<div style="display: table; color: black;border: black 4px solid;padding:10px;text-align:center;margin-bottom:20px;"><div style="line-height: initial; font-weight: bold;">Get £5 OFF at checkout</div></div>';
        private static $banner_square_full_img = '<div style="display: table; color: black;border: black 4px solid;padding:10px;text-align:center;margin-bottom:20px;width: fit-content;padding-left: 40px;padding-right: 40px;"><div style="line-height: initial; font-weight: bold;">Get £5 OFF at checkout</div></div>';

        /**
		 * The reference the *Singleton* instance of this class.
		 *
		 * @var $instance
		 */
        private static $instance = null;

        /**
		 * Returns the *Singleton* instance of this class.
		 *
		 * @return self::$instance The *Singleton* instance.
		 */
        public static function get_instance()
        {
			if (is_null(self::$instance)) {
				self::$instance = new self;
			}
			return self::$instance;
        }

        /**
		 * Private clone method to prevent cloning of the instance of the
		 * *Singleton* instance.
		 *
		 * @return void
		 */
		private function __clone() {
		}

		/**
		 * Private unserialize method to prevent unserializing of the *Singleton*
		 * instance.
		 *
		 * @return void
		 */
		private function __wakeup() {
        }
        
        /**
         * Constructor
         */
        public function __construct() {
            // Load Amplitude
            require_once WCMOONA_BASE_PATH . '/includes/moona_amplitude.php';

            // Check WooCommerce is active
            if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || in_array( 'woocommerce/woocommerce.php', array_keys( get_site_option( 'active_sitewide_plugins' ) ) ) ) {
                
                // Load Cleever plugin
                add_action( 'plugins_loaded', array( $this, 'wcmoona_plugin_to_load' ), 0 );

                // Add link to the Cleever settings
                add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), array( $this, 'add_action_settings_link' ) );

                // Amplitude : Checkout view
                add_action( 'woocommerce_after_checkout_form', array( $this, 'after_checkout_form_loaded'), 10, 1 );

                // Amplitude : Click CTA
                add_action( 'woocommerce_checkout_order_processed', array( $this, 'action_checkout_order_processed'), 10, 1 );

                // Display discount at checkout
                add_action( 'woocommerce_review_order_before_order_total', array( $this, 'review_order_before_order_total_callback' ), 10, 0 );
                add_action( 'woocommerce_review_order_after_order_total', array( $this, 'review_order_after_order_total_callback' ), 10, 0 );
                add_filter( 'woocommerce_cart_total', array( $this, 'linethrough_price' ), 10, 1 );
                add_filter( 'woocommerce_get_order_item_totals',  array( $this, 'display_moona_discount_new_row' ), 10, 2 );
                add_filter( 'woocommerce_get_formatted_order_total',  array( $this, 'display_moona_order_total' ), 10, 4 );
                add_action( 'wp_ajax_moona_store_payload', array( $this, 'moona_store_payload' ) );
                add_action( 'wp_ajax_nopriv_moona_store_payload', array( $this, 'moona_store_payload' ) );
                add_action( 'template_redirect', array( $this, 'moona_flush_session' ) );
                add_action( 'template_redirect', array( $this, 'get_shipping_slot_infos' ) );

                // Order status changed
                add_action( 'woocommerce_order_status_changed', array( $this, 'action_order_status_changed'), 10, 4 );
                // Other hook ways
                // woocommerce_payment_complete
                // woocommerce_new_order
                // woocommerce_order_status_* => woocommerce_order_status_completed
                // woocommerce_order_status_pending
                // woocommerce_order_status_failed
                // woocommerce_order_status_on-hold
                // woocommerce_order_status_processing
                // woocommerce_order_status_completed
                // woocommerce_order_status_refunded
                // woocommerce_order_status_cancelled
                add_action( 'woocommerce_new_order', array( $this, 'action_create_moona_order'), 10, 2 );
                
                // Init Cron job & add new 'every 10 minutes' schedule'
                // Schedule an event unless already scheduled
                add_action('wp', array( $this, 'create_moona_cron_jobs') );
                // Define a cron job interval if it doesn't exist
                add_filter( 'cron_schedules', array( $this, 'cron_every_ten_minutes') );
                add_action('moona_cron', array( $this, 'update_orders') );

                add_action('init', array( $this, 'update_plugin_version' ), 10, 0 );

                // Save Cleever position on Payments tabs > Save settings
                add_action( "woocommerce_update_options_checkout", array( $this, 'action_woocommerce_settings_save_checkout' ), 10, 1 ); 

                // Checkbox
               add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'single_product_checkbox' ), 99, 0 );
               add_action( 'template_redirect', array( $this, 'checkbox_set_mp1_as_default_gateway' ) );
               add_action( 'woocommerce_thankyou', array( $this, 'unset_checkbox_cookie_after_order' ), 10, 1 );
               add_action( 'template_redirect', array( $this, 'unset_cookie_if_mp2_cancel' ) );
               add_action( 'wp_ajax_product_checkbox_onchange_event', array( $this, 'product_checkbox_onchange_event' ) );
               add_action( 'wp_ajax_nopriv_product_checkbox_onchange_event', array( $this, 'product_checkbox_onchange_event' ) );
               add_action( 'woocommerce_add_to_cart', array( $this, 'product_checkbox_added_to_cart' ) );
               add_action( 'woocommerce_widget_shopping_cart_total', array( $this, 'update_minicart_subtotal_html_checkbox_product' ), 10 );
               add_filter( 'woocommerce_cart_totals_order_total_html', array( $this, 'update_cart_total_html_checkbox_product' ), 10, 1 );
               add_action( 'after_setup_theme', array( $this, 'remove_actions' ) );
               add_filter( 'woocommerce_get_price_html', array( $this, 'checkbox_product_filter_html_price' ), 100, 2 );
               add_filter( 'woocommerce_grouped_price_html', array( $this, 'checkbox_product_filter_html_price_grouped' ), 10, 3 );

            }

            add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

            // Customize banners
            $this->moona_options = get_option('woocommerce_discount-payment-moona_settings');
            $this->customize_banners();

            // Banners
            add_action( 'woocommerce_before_shop_loop', array( $this, 'action_woocommerce_before_shop_loop' ), 10, 2 ); 
            add_action( 'woocommerce_before_single_product', array( $this, 'action_woocommerce_before_single_product' ), 10, 2 ); 
            add_action( 'woocommerce_before_cart_table', array( $this, 'action_woocommerce_before_cart_table' ), 10, 2 ); 
            add_action( 'woocommerce_before_checkout_form', array( $this, 'action_woocommerce_before_checkout_form' ), 10, 2 ); 
            add_action( 'woocommerce_thankyou', array( $this, 'action_woocommerce_thankyou' ), 10, 1 );

            add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'action_woocommerce_before_add_to_cart_form' ), 10, 2 );
            add_action( 'woocommerce_proceed_to_checkout', array( $this, 'action_woocommerce_proceed_to_checkout' ), 10, 2 ); 
            add_action( 'woocommerce_review_order_before_payment', array( $this, 'action_woocommerce_review_order_before_payment' ), 10, 2 );
            add_action( 'woocommerce_mini_cart_contents', array( $this, 'action_woocommerce_mini_cart_contents' ), 10, 2 ); 

            add_action( 'woocommerce_after_mini_cart', array( $this, 'action_woocommerce_after_mini_cart' ), 10, 2 );

            if ( session_status() === PHP_SESSION_NONE ) {
                session_start();
            }
            if ( isset( $_SESSION['moona_shipping_infos_first_load'] ) )
                $this->shipping_infos_first_load_done = $_SESSION['moona_shipping_infos_first_load'];
            else
                $this->shipping_infos_first_load_done = false;


            if ( isset( $_SESSION['moona_shipping_infos_refresh_shipping'] ) ) {
                $this->call_get_shipping_slot_infos_on_update = $_SESSION['moona_shipping_infos_refresh_shipping'];
            } else {
                $this->call_get_shipping_slot_infos_on_update = false;
            }

            if ( isset( $_SESSION['moona_shopper_infos'] ) ) {
                $this->shopper_infos = $_SESSION['moona_shopper_infos'];
            } else {
                $this->shopper_infos = NULL;
            }

            if ( isset( $_SESSION['moona_shipping_infos_new_user'] ) ) {
                $this->shipping_infos_new_user = $_SESSION['moona_shipping_infos_new_user'];
            } else {
                $this->shipping_infos_new_user = array(
                    'title'                 => __( 'Save £5 on shipping', 'discount-payment-moona' ),
                    'title_free'            => __( 'Free shipping', 'discount-payment-moona' ),
                    'min_amount_to_display' => 0,
                    'discount'              => 500,
                    'description'           => __( 'Get free shipping up to £5. 15 day trial then £24.90 per month. Cancel anytime.', 'discount-payment-moona' ),
                    'description_free'      => __( 'Get free shipping. 15 day trial then £24.90 per month. Cancel anytime.', 'discount-payment-moona' ),
                    'display_slot'          => false
                );
            }

            if ( isset( $_SESSION['moona_shipping_infos_returning_user'] ) ) {
                $this->shipping_infos_returning_user = $_SESSION['moona_shipping_infos_returning_user'];
            } else {
                $this->shipping_infos_returning_user = array(
                    'title'                 => __( 'Save £5 on shipping', 'discount-payment-moona' ),
                    'title_free'            => __( 'Free shipping', 'discount-payment-moona' ),
                    'min_amount_to_display' => 6000,
                    'discount'              => 500,
                    'description'           => __( 'This order is eligible to £5 rebate on shipping.', 'discount-payment-moona' ),
                    'description_free'      => __( 'This order is eligible to free shipping', 'discount-payment-moona' ),
                    'display_slot'          => false
                );                
            }

            if ( isset( $_SESSION['product_slot_data'] ) ) {
                $this->product_slot_data = $_SESSION['product_slot_data'];
            } else {
                $this->product_slot_data = false;
            }


            add_action( 'wp_ajax_get_shopper_infos_ajax', array( $this, 'get_shopper_infos_ajax' ) );
            add_action( 'wp_ajax_nopriv_get_shopper_infos_ajax', array( $this, 'get_shopper_infos_ajax' ) );

            add_filter( 'woocommerce_package_rates', array( $this, 'add_shipping_slot' ), 100, 2 );
            add_filter( 'woocommerce_available_payment_gateways', array( $this, 'show_active_gateways' ) ,1 );
            add_filter( 'wp_loaded', array( $this, 'disable_shipping_cache_conditionnal' ) );
            add_filter( 'woocommerce_cart_shipping_packages', array( $this, 'woocommerce_shipping_rate_cache_invalidation' ), 100 );
            add_filter( 'wp_loaded', array( $this, 'hide_associated_method' ) );

        }

        public function remove_actions() {
            remove_action( 'woocommerce_widget_shopping_cart_total', 'woocommerce_widget_shopping_cart_subtotal', 10 );
        }

        public function display_moona_order_total( $formatted_total, $order, $tax_display, $display_refunded ) {

            // Display discount on Cleever discount for order-pay and order-received
            $displayDiscount = $order->get_meta('_has_moona_discount');
            // Display discount if &disc=1
            $displayDiscountGet = (isset($_GET['disc']) && $_GET['disc'] == 1) ? true : false;
            
            if (
                (is_wc_endpoint_url('order-pay') || is_wc_endpoint_url('order-received')) &&
                ($displayDiscountGet || $displayDiscount === 'true')
             ) {
                $discount = (empty($order->get_meta('_moona_discount_amount'))) ? 500 : intval($order->get_meta('_moona_discount_amount'));
                $formatted_total = '<span style="text-decoration:line-through;">'. $formatted_total .'</span> ';
                $new_total = wc_price( $order->get_total() - ($discount / 100), array( 'currency' => $order->get_currency() ) );
                $new_total = str_replace('class="woocommerce-Price-amount amount"', 'class="woocommerce-Price-amount amount" style="color:red; font-weight: 600;"', $new_total);
                $formatted_total .= $new_total;
            }

            return $formatted_total;
        }

        public function moona_store_payload() {

            if ( ! isset( $_POST['payload'] ) )
                wp_json_send_error();

            if ( session_status() === PHP_SESSION_NONE ) {
                session_start();
            }

            $_SESSION['moona_iframe_payload'] = $_POST['payload'];

            ob_start();
            $this->review_order_after_order_total_callback();
            $html = ob_get_contents();
            ob_end_clean();

            wp_send_json_success( array( 'html' => $html ) );

        }

        public function moona_flush_session() {

            if ( is_admin() || ! is_checkout() || defined( 'DOING_AJAX' ) )
                return;

            if ( session_status() === PHP_SESSION_NONE ) {
                session_start();
            }

            if ( isset( $_SESSION['moona_iframe_payload']) )
                unset( $_SESSION['moona_iframe_payload'] );

            if ( isset( $_SESSION['moona_email']) )
                unset( $_SESSION['moona_email'] );

            if ( isset( $_SESSION['moona_shipping_infos_first_load']) ) {
                unset( $_SESSION['moona_shipping_infos_first_load'] );
                $this->shipping_infos_first_load_done = false;
            }

            if ( isset( $_SESSION['moona_shipping_infos_new_user'] ) )
                unset( $_SESSION['moona_shipping_infos_new_user'] );

            if ( isset( $_SESSION['moona_shipping_infos_returning_user'] ) )
                unset( $_SESSION['moona_shipping_infos_returning_user'] );

            if ( isset( $_SESSION['moona_shipping_infos_refresh_shipping'] ) )
                unset( $_SESSION['moona_shipping_infos_refresh_shipping'] );

            if ( isset( $_SESSION['moona_shopper_infos'] ) )
                unset( $_SESSION['moona_shopper_infos'] );
            
        }

        function wp_enqueue_scripts() {

            $this->moona_options = get_option('woocommerce_discount-payment-moona_settings');
            if ( $this->moona_options === false ) {
                return;
            }

            // Load css file
            wp_enqueue_style('wc_moona_css', WCMOONA_BASE_URL . 'assets/css/moona.css', array(), WCMOONA_FILE_VERSION);

            if ( is_checkout() || is_wc_endpoint_url('order-pay') || is_singular( 'product' ) ) {
                // Allow to call the script moona.js to update the checkout text of Cleever
                wp_enqueue_script('moona-js', WCMOONA_BASE_URL . 'assets/js/moona.js', array( 'jquery' ), WCMOONA_FILE_VERSION, false );

                $js_array = array( 'ajax_url' => admin_url( 'admin-ajax.php' ) );

                $enabled = ( isset( $this->moona_options['product_slot_enabled'] ) && $this->moona_options['product_slot_enabled'] == 'yes' ) ? true : false;
                if ( $enabled  ) {
                    $js_array['product_slot'] = $this->webservice_get_checkbox_data();
                }
                // Add JS variable to allow to send POST AJAX message
                wp_localize_script( 'moona-js', 'ajax_object', $js_array );
            }
        }

        function display_moona_discount_new_row( $total_rows, $order ) {
            
            $displayDiscount = $order->get_meta('_has_moona_discount');
            if  (   
                    ($order->get_payment_method() === 'discount-payment-moona' || $order->get_payment_method() === 'discount-payment-main-moona') && 
                    ($displayDiscount === 'true')
                ) {

                // The altered code for the thankyou page
                if ( is_wc_endpoint_url('order-received') ) {
                    $source = 'order-summary';
                } 
                // Only on emails notifications
                else if ( ! is_wc_endpoint_url() || ! is_admin() ) {
                    $source = 'order-email';
                }
                
                $this->display_discount = $this->get_discount_text($order->get_total(), $source);

                if ($this->display_discount && $this->display_discount['display_mode'] === 'text_with_discount' && $this->display_discount['display_with_discount']) {
                    // No need anymore because it's already made with display_moona_order_total function
                    // if( isset($total_rows['order_total']) ) {
                    //     $total_rows['order_total']['value'] = '<span style="text-decoration:line-through">' . $total_rows['order_total']['value'] . '</span>';
                    // }
        
                    $total_rows['moona_discount'] = array(
                        'label' => $this->display_discount['text_with_discount'],
                        'value' => $this->display_discount['total_with_discount']
                    );
                }
            }
            
            return $total_rows;
        }

        function review_order_before_order_total_callback() {

            $chosen_payment_method = WC()->session->get('chosen_payment_method');
            if ( $this->moona_options === false || $chosen_payment_method !== 'discount-payment-moona') {
                return;
            }

            $this->display_discount = $this->get_discount_text(WC()->cart->total, 'checkout');
        }

        function get_discount_text($total, $source) {
            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'lang' => get_locale(),
                'price' => round( number_format($total, 2 , '.' , '' ) * 100),
                'currency_code' => get_woocommerce_currency(),
                'currency_symbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'source' => $source
            );

            if (isset($this->moona_options['merchant_id'])) {
                $body['merchant_id'] = $this->moona_options['merchant_id'];
            }

            $url = ( $this->moona_options['test_mode'] === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT;
            $url .= 'ecommerce/display_discount';
            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->moona_options['test_mode'] === 'yes' ) ? $this->moona_options['moona_sk_test'] : $this->moona_options['moona_sk_live']
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
                $this->log( 'error on get_discount_text() process' );
            } else {
                // {
                //     display_mode: 'text_with_discount', // or text_without_discount => allow to know if we display the text_with_discount or the other one
                //     display_with_discount: true, 
                //     text_with_discount: '<span style="color:red;">New Total With Cleever</span>', 
                //     total_with_discount: `<span style="color:red;">${currency_symbol} ${new_total}</span>`,
                //     display_without_discount: true,
                //     text_without_discount: '<span style="color:red;">£5 off with Cleever on orders above £10</span>' // Add items worth £x, and get £5 off with
                // }
                return json_decode( $response['body'], true );
            }
        }

        function linethrough_price($wc_price) {

            if ( ! is_checkout() )
                return $wc_price;

            $chosen_payment_method = WC()->session->get('chosen_payment_method');

            if ( session_status() === PHP_SESSION_NONE ) {
                session_start();
            }

            if ($chosen_payment_method !== 'discount-payment-main-moona' && isset( $_SESSION['moona_iframe_payload'] ) ) {
                $_SESSION['moona_iframe_payload']['is_checked'] = 'false';
            }

            if ( $this->display_discount
                    && $chosen_payment_method === 'discount-payment-moona' 
                    && $this->display_discount['display_mode'] === 'text_with_discount' 
                    && $this->display_discount['display_with_discount'] ) {
                return '<span style="text-decoration:line-through">'. $wc_price .'</span>';
            } else if ( $chosen_payment_method === 'discount-payment-main-moona' && isset( $_SESSION['moona_iframe_payload'] ) ) {
                $iframe_payload = $_SESSION['moona_iframe_payload'];
                if ( $iframe_payload['type'] === 'discount' && $iframe_payload['is_checked'] === 'true' ) {
                    return '<span style="text-decoration:line-through">'. $wc_price .'</span>';
                }
            }
            return $wc_price;
        }

        function action_woocommerce_settings_save_checkout( $array ) {
            $url = wc_get_current_admin_url();
            if (strpos($url, 'tab=checkout') !== false && strpos($url, 'section=') === false) {
                $GLOBALS['wc_moona_amplitude']->saveMoonaPosition();
            }
        }

        function review_order_after_order_total_callback() {

            if ( ! ( is_checkout() || wp_doing_ajax() ) )
                return;

            $chosen_payment_method = WC()->session->get('chosen_payment_method'); 

            if ( session_status() === PHP_SESSION_NONE ) {
                session_start();
            }
            
            if ( $this->display_discount && $chosen_payment_method === 'discount-payment-moona' ) {
                if ($this->display_discount['display_mode'] === 'text_with_discount' && $this->display_discount['display_with_discount']) {
                    echo '<tr class="order-total"><th>'. $this->display_discount['text_with_discount'] .'</th><th>'. $this->display_discount['total_with_discount'] .'</th></tr>';
                } 
                else if ($this->display_discount['display_mode'] === 'text_without_discount' && $this->display_discount['text_without_discount']) {
                    echo '<tr class="order-total"><th colspan="2" >'. $this->display_discount['text_without_discount'] .'</th></tr>';
                }

            } else if ( $chosen_payment_method === 'discount-payment-main-moona' && isset( $_SESSION['moona_iframe_payload'] ) ) {
                $iframe_payload = $_SESSION['moona_iframe_payload'];
                if ( $iframe_payload['type'] === 'discount' && $iframe_payload['is_checked'] === 'true' ) {
                    $cart_total = WC()->cart->get_cart_contents_total() + WC()->cart->get_shipping_total(); // Get total including shipping
                    $cart_total = round( number_format($cart_total, 2 , '.' , '' ) * 100);
                    $cart_total -= intval( $iframe_payload['discount_amount'] );
                    $cart_total = round( $cart_total / 100, 2 );
                    if ( $cart_total < 0 )
                        $cart_total = 0;
                    $cart_total = number_format( $cart_total, 2, '.', ' ' );
                    echo '<tr class="order-total new-order-total"><th style="font-weight: bold;color: red;">'. __( 'New total', 'discount-payment-moona' ) .'</th><th style="font-weight: bold;color: red;">£'. $cart_total .'</th></tr>';
                }
            }
        }

        /**
         * Customize banners according to WS
         */
        function customize_banners() {

            $this->moona_options = get_option('woocommerce_discount-payment-moona_settings');
            if ( $this->moona_options === false ) {
                return;
            }

            // TODO => Banners independant ?
            // Need to have => MP + MD || MD 
            if ( $this->moona_options['enabled'] !== 'yes' ) {
                return false;
            }
            // if ( empty($this->moona_options['enabled_main']) || $this->moona_options['enabled_main'] !== 'yes' ) {
            //     return false;
            // }

            // Update banners every 24h
            if ( isset($this->moona_options['_timestamp_update_banners']) ) {
                if ( ($this->moona_options['_timestamp_update_banners'] + 24*60*60) > time() ) {
                    return;
                }
            }
            $options = $this->moona_options;
            $options['_timestamp_update_banners'] = time();
            
            // Fetch the banners
            $locale = get_locale();
            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'lang' => $locale
            );

            if (isset($this->moona_options['merchant_id'])) {
                $body['merchant_id'] = $this->moona_options['merchant_id'];
            }

            $url = ( $this->moona_options['test_mode'] === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT;
            $url .= 'ecommerce/banners';
            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->moona_options['test_mode'] === 'yes' ) ? $this->moona_options['moona_sk_test'] : $this->moona_options['moona_sk_live']
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
                $this->log( 'error on customize_banners() process : ' . print_r($response, true) );
            } else {
                $body = json_decode( $response['body'], true );
                $this->banners = $body['banners'];

                // Save the banners
                $options['_banner_top_shop_archives_html'] = $this->banners['banner_top_shop_archives'];
                $options['_banner_top_product_page_html'] = $this->banners['banner_top_product_page'];
                $options['_banner_top_cart_page_html'] = $this->banners['banner_top_cart_page'];
                $options['_banner_top_checkout_page_html'] = $this->banners['banner_top_checkout_page'];
                $options['_banner_top_thankyou_page_html'] = $this->banners['banner_top_thankyou_page'];
                $options['_banner_above_cta_product_page_html'] = $this->banners['banner_above_cta_product_page'];
                $options['_banner_above_cta_cart_page_html'] = $this->banners['banner_above_cta_cart_page'];
                $options['_banner_above_cta_checkout_page_html'] = $this->banners['banner_above_cta_checkout_page'];
                $options['_banner_above_cta_minicart_html'] = $this->banners['banner_above_cta_minicart'];
                $options['_banner_square_cart_page_html'] = $this->banners['banner_square_cart_page'];
            }
            update_option('woocommerce_discount-payment-moona_settings', $options);
        }

        // V1.0.0 : Retro-compatibility
        // Update the merchant plugin version
        function update_plugin_version() {

            $this->moona_options = get_option('woocommerce_discount-payment-moona_settings');
            if ( $this->moona_options === false ) {
                return;
            }

            // Check the version
            if ( isset($this->moona_options['_merchant_plugin_version']) ) {
                if ( $this->moona_options['_merchant_plugin_version'] === WCMOONA_PLUGIN_VERSION ) {
                    return;
                }
            }
            $options = $this->moona_options;
            $options['_merchant_plugin_version'] = WCMOONA_PLUGIN_VERSION;
            update_option('woocommerce_discount-payment-moona_settings', $options);

            wcmoona_plugin_status('updated');
        }

        // To know if moona is activated
        function is_moona_activated() {
            $this->moona_options = get_option('woocommerce_discount-payment-moona_settings');
            if ( $this->moona_options === false ) {
                return false;
            }
            if ( $this->moona_options['enabled'] !== 'yes' ) {
                return false;
            }
            if ( !in_array( get_woocommerce_currency(), array( 'GBP' ) ) ) {
                return false;
            }
            return true;
        }

        function action_woocommerce_before_shop_loop() {
            if (isset($this->moona_options['banner_top_shop_archives']) && $this->moona_options['banner_top_shop_archives'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_top_shop_archives_html'])) {
                    echo $this->moona_options['_banner_top_shop_archives_html'];
                } else {
                    echo self::$banner_html;
                }
            }
        }

        function action_woocommerce_before_single_product() {
            if (isset($this->moona_options['banner_top_product_page']) && $this->moona_options['banner_top_product_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_top_product_page_html'])) {
                    echo $this->moona_options['_banner_top_product_page_html'];
                } else {
                    echo self::$banner_html;
                }
            }
        }

        function action_woocommerce_before_cart_table() {
            if (isset($this->moona_options['banner_top_cart_page']) && $this->moona_options['banner_top_cart_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_top_cart_page_html'])) {
                    echo $this->moona_options['_banner_top_cart_page_html'];
                } else {
                    echo self::$banner_html;
                }
            }
        }

        function action_woocommerce_before_checkout_form() {
            if (isset($this->moona_options['banner_top_checkout_page']) && $this->moona_options['banner_top_checkout_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_top_checkout_page_html'])) {
                    echo $this->moona_options['_banner_top_checkout_page_html'];
                } else {
                    echo self::$banner_html;
                }
            }
        }

        function action_woocommerce_thankyou( $order_id ) {
            if (isset($this->moona_options['banner_top_thankyou_page']) && $this->moona_options['banner_top_thankyou_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_top_thankyou_page_html'])) {
                    echo $this->moona_options['_banner_top_thankyou_page_html'];
                } else {
                    echo self::$banner_post_order_html;
                }
            }

            // Order confirmation
            // When the shopper see the order confirmation page
            // Amplitude event
            if ( ! $order_id ) {
                return;
            }

            $order = wc_get_order( $order_id );            
            $shortVersion = explode(".", WC_VERSION);
            
            $paymentMethodID = $order->get_payment_method();
            if ($paymentMethodID === 'discount-payment-moona') {
                $paymentMethodID = 'moona-discount';
            } else if ($paymentMethodID === 'discount-payment-main-moona') {
                $paymentMethodID = 'moona-payment';
            }

            $eventProperties = array(
                'merchantSite' =>  get_bloginfo( 'name' ),
                'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                'orderID' => $order_id,
                'currency' => get_woocommerce_currency(),
                'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'orderTotalValue' => $order->get_total(),
                'paymentMethodID' => $paymentMethodID,
                'paymentMethodLabel' => $order->get_payment_method_title(),
                'orderStatus' => $order->get_status(),
                'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                'pluginVersion' => WCMOONA_PLUGIN_VERSION
            );

            if (isset($this->moona_options['merchant_id'])) {
                $eventProperties['merchantID'] = $this->moona_options['merchant_id'];
            }

            $GLOBALS['wc_moona_amplitude']->logEvent('View - Order confirmation', $eventProperties);
        }

        function action_woocommerce_before_add_to_cart_form() {
            if (isset($this->moona_options['banner_above_cta_product_page']) && $this->moona_options['banner_above_cta_product_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_above_cta_product_page_html'])) {
                    echo $this->moona_options['_banner_above_cta_product_page_html'];
                } else {
                    echo self::$banner_above_cta_text_html;
                }
            }
        }

        function action_woocommerce_proceed_to_checkout() {
            if (isset($this->moona_options['banner_above_cta_cart_page']) && $this->moona_options['banner_above_cta_cart_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_above_cta_cart_page_html'])) {
                    echo $this->moona_options['_banner_above_cta_cart_page_html'];
                } else {
                    echo self::$banner_center_above_cta_html;
                }
            }
        }

        function action_woocommerce_review_order_before_payment() {
            if (isset($this->moona_options['banner_above_cta_checkout_page']) && $this->moona_options['banner_above_cta_checkout_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_above_cta_checkout_page_html'])) {
                    echo $this->moona_options['_banner_above_cta_checkout_page_html'];
                } else {
                    echo self::$banner_center_above_cta_html;
                }
            }
        }

        function action_woocommerce_mini_cart_contents() {
            if (isset($this->moona_options['banner_above_cta_minicart']) && $this->moona_options['banner_above_cta_minicart'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_above_cta_minicart_html'])) {
                    echo $this->moona_options['_banner_above_cta_minicart_html'];
                } else {
                    echo self::$banner_center_above_cta_html;
                }
            }
        }

        function action_woocommerce_after_mini_cart() {
            if (isset($this->moona_options['banner_square_cart_page']) && $this->moona_options['banner_square_cart_page'] === 'yes' && $this->is_moona_activated()) {
                if (isset($this->moona_options['_banner_square_cart_page_html'])) {
                    echo $this->moona_options['_banner_square_cart_page_html'];
                } else {
                    echo self::$banner_square_full_img;
                }
            }
        }

        // Plugins loaded
        function wcmoona_plugin_to_load() {
            // i18n
            load_plugin_textdomain( 'discount-payment-moona', false, WCMOONA_BASE_NAME . '/languages/' );

            // Check if the class exist
            if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
                add_action( 'admin_notices', array( $this, 'wcmoona_woocommerce_fallback_notice' ) );
                return;
            }

            // This hook registers our PHP class as a WooCommerce payment gateway
            add_filter( 'woocommerce_payment_gateways', array( $this, 'add_moona_gateway_class' ) );
        }

        // Create order
        function action_create_moona_order( $order_id, $order ) {

            foreach( $order->get_items( 'shipping' ) as &$shipping_item ) {
              if ( $shipping_item->get_instance_id() == 999 ) {
                  $moona_options = get_option('woocommerce_discount-payment-moona_settings');
                  if ( array_key_exists( 'shipping_slot_associated_method', $moona_options ) ) {
                      $associated_id = $moona_options['shipping_slot_associated_method'];

                      $zones = WC_Shipping_Zones::get_zones();
                      $shipping_methods = array_column( $zones, 'shipping_methods' );
                      foreach ( $shipping_methods[0] as $key => $class ) {
                          if ( $class->instance_id == $associated_id ) {
                              $copy_shipping_rate = new WC_Shipping_Rate( $class->id );
                              $shipping_item->set_shipping_rate( $copy_shipping_rate );

                              $is_returning_user = $this->is_returning_user();
                              $shipping_infos = $is_returning_user ? $this->get_shipping_infos_returning_user() : $this->get_shipping_infos_new_user();

                              $shipping_item->set_method_title( $class->title . ' via ' . $shipping_infos['label'] );
                              $shipping_item->save();
                              update_post_meta( $order->get_id(), '_mship', 1 );
                              break;
                          }
                      }
                  }
              }
            }

            $this->amplitude_order_status($order_id, '', $order->get_status(), $order);
            // if (($order->get_payment_method() === 'discount-payment-moona' || $order->get_payment_method() === 'discount-payment-main-moona') &&
            //     ($order->get_status() === 'pending')) {
            // }
        }

        // Update order status
        // When the order status is updated (Pending payment - Failed - Processing - Completed - On hold - Canceled - Refunded)
        function action_order_status_changed( $order_id, $old_status, $new_status, $order ) {
            $this->amplitude_order_status($order_id, $old_status, $new_status, $order);
        }

        private function amplitude_order_status($order_id, $old_status, $new_status, $order) {
            // Amplitude event
            $shortVersion = explode(".", WC_VERSION);

            $paymentMethodID = $order->get_payment_method();
            if ($paymentMethodID === 'discount-payment-moona') {
                $paymentMethodID = 'moona-discount';
            } else if ($paymentMethodID === 'discount-payment-main-moona') {
                $paymentMethodID = 'moona-payment';
            }

            $eventProperties = array(
                'merchantSite' =>  get_bloginfo( 'name' ),
                'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                'orderID' => $order_id,
                'currency' => get_woocommerce_currency(),
                'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'orderTotalValue' => $order->get_total(),
                'paymentMethodID' => $paymentMethodID,
                'paymentMethodLabel' => $order->get_payment_method_title(),
                'orderStatus' => $new_status, // $new_status OR $order->get_status()
                'orderStatusOld' => $old_status,
                'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                'pluginVersion' => WCMOONA_PLUGIN_VERSION
            );

            if (isset($this->moona_options['merchant_id'])) {
                $eventProperties['merchantID'] = $this->moona_options['merchant_id'];
            }

            $moonaUserId = $order->get_meta('_moona_user_id');
            $GLOBALS['wc_moona_amplitude']->logEvent('Order - Status', $eventProperties, $moonaUserId);
        }

        // CTA Place order is clicked => order is created (pending)
        function action_checkout_order_processed( $order_id ) {
            $order = wc_get_order( $order_id );

            // Amplitude event
            $shortVersion = explode(".", WC_VERSION);

            $paymentKeys = array_keys( WC()->payment_gateways->get_available_payment_gateways() );
            $moonaDiscountPosition = array_search( 'discount-payment-moona',  $paymentKeys) ;
            $moonaPaymentPosition = array_search( 'discount-payment-main-moona', $paymentKeys) ;
            $moonaDiscountPosition = ($moonaDiscountPosition !== false) ? $moonaDiscountPosition : -1;
            $moonaPaymentPosition = ($moonaPaymentPosition !== false) ? $moonaPaymentPosition : -1;

            $paymentMethodID = $order->get_payment_method();
            if ($paymentMethodID === 'discount-payment-moona') {
                $paymentMethodID = 'moona-discount';
            } else if ($paymentMethodID === 'discount-payment-main-moona') {
                $paymentMethodID = 'moona-payment';
            }
            
            $eventProperties = array(
                'merchantSite' =>  get_bloginfo( 'name' ),
                'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                'moonaPaymentPosition' => $moonaPaymentPosition,
                'moonaDiscountPosition' => $moonaDiscountPosition,
                'orderID' => $order_id,
                'currency' => get_woocommerce_currency(),
                'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'orderTotalValue' => $order->get_total(),
                'paymentMethodID' => $paymentMethodID,
                'paymentMethodLabel' => $order->get_payment_method_title(),
                'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                'pluginVersion' => WCMOONA_PLUGIN_VERSION,
                'orderStatus' => $order->get_status() // Not mandatory because it will be always Pending
            );

            if (isset($this->moona_options['merchant_id'])) {
                $eventProperties['merchantID'] = $this->moona_options['merchant_id'];
            }

            $GLOBALS['wc_moona_amplitude']->logEvent('Click - Merchant checkout', $eventProperties);
        }

        // Checkout view
        function after_checkout_form_loaded( $checkout ){
            // print_r($checkout);
            // print_r(WC()->session);
            // print_r(get_current_user_id());
            // Only for connected user
            // print_r(wp_get_session_token());

            if (!$this->is_moona_activated()) {
                return false;
            }

            // TO BE RETRO COMPATIBLE : Save the merchant id
            $merchantId = null;
            if (!isset($this->moona_options['merchant_id'])) {

                $body = array(
                    'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                    'ecommerce_version' => WC_VERSION,
                    'plugin_version' => WCMOONA_PLUGIN_VERSION,
                    'api_version' => WCMOONA_API_VERSION
                );

                $url = (( $this->moona_options['test_mode'] === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT);
                $url .= 'ecommerce/merchant';

                $response = wp_remote_post( $url, array(
                                    'method' => 'POST',
                                    'timeout' => 20,
                                    'headers' => array(
                                        'Content-Type' => 'application/json; charset=utf-8',
                                        'x-api-key' => ( $this->moona_options['test_mode'] === 'yes' ) ? $this->moona_options['moona_sk_test'] : $this->moona_options['moona_sk_live']
                                    ),
                                    'body' => json_encode($body)
                                )
                            );

                if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
                    $this->log( 'error on customize_banners() process : ' . print_r($response, true) );
                } else {
                    $body = json_decode( $response['body'], true );
                    $options = $this->moona_options;
                    $options['merchant_id'] = $body['merchantId'];
                    update_option('woocommerce_discount-payment-moona_settings', $options);
                    $merchantId = $options['merchant_id'];
                }
            } else {
                $merchantId = $this->moona_options['merchant_id'];
            }

            // Amplitude event
            $shortVersion = explode(".", WC_VERSION);
            
            $paymentKeys = array_keys( WC()->payment_gateways->get_available_payment_gateways() );
            $moonaDiscountPosition = array_search( 'discount-payment-moona',  $paymentKeys) ;
            $moonaPaymentPosition = array_search( 'discount-payment-main-moona', $paymentKeys) ;
            $moonaDiscountPosition = ($moonaDiscountPosition !== false) ? $moonaDiscountPosition : -1;
            $moonaPaymentPosition = ($moonaPaymentPosition !== false) ? $moonaPaymentPosition : -1;

            $eventProperties = array(
                'merchantSite' => get_bloginfo( 'name' ),
                'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                'moonaPaymentPosition' => $moonaPaymentPosition,
                'moonaDiscountPosition' => $moonaDiscountPosition,
                'currency' => get_woocommerce_currency(),
                'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'orderTotalValue' => WC()->cart->total,
                'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                'pluginVersion' => WCMOONA_PLUGIN_VERSION
            );

            if (isset($merchantId)) {
                $eventProperties['merchantID'] = $merchantId;
            }

            $GLOBALS['wc_moona_amplitude']->logEvent('View - Merchant checkout', $eventProperties);
        }

        private function get_shopper_infos_webservice( $email ) {

            $body = array(
                'email' => $email,
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
            );

            if ( isset( $this->moona_options['merchant_id'] ) ) {
                $body['merchant_id'] = $this->moona_options['merchant_id'];
            }

            $url = ( $this->moona_options['test_mode'] === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT;
            $url .= 'ecommerce/get_shopper_infos';
            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->moona_options['test_mode'] === 'yes' ) ? $this->moona_options['moona_sk_test'] : $this->moona_options['moona_sk_live']
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
                $this->log( 'error on get_shopper_info() process' );
                return false;
            } else {
                $body = json_decode( $response['body'], true );
                if ( is_array( $body ) && array_key_exists( 'body', $body) ) {
                    $body = $body['body'];
                    $this->shopper_infos = $body;
                    if ( session_status() === PHP_SESSION_NONE ) {
                        session_start();
                    }
                    $_SESSION['moona_shopper_infos'] = $body;
                }
                return true;
            }
        }

        public function get_shopper_infos_ajax() {

            if ( ! isset( $_POST['email'] ) ) {
                wp_send_json_error();
            }

            if ( session_status() === PHP_SESSION_NONE ) {
                session_start();
            }

            $_SESSION['moona_email'] = $_POST['email'];

            $ret = $this->get_shopper_infos_webservice( $_POST['email'] );
            if ( $ret ) {
                wp_send_json_success( $this->shopper_infos );
            } else {
                wp_send_json_error();
            }

        }

        public function get_shopper_infos() {

            if ( ! $this->shopper_infos || $this->shopper_infos == NULL ) {
                $billing_email = WC()->customer->get_billing_email();

                if ( session_status() === PHP_SESSION_NONE ) {
                    session_start();
                }
                if ( isset( $_SESSION['moona_email'] ) ) {
                    $billing_email = $_SESSION['moona_email'];
                }

                $this->get_shopper_infos_webservice( $billing_email );
            }

            return $this->shopper_infos;

        }

        public function get_shopper_infos_formatted() {

            $shopper_infos = $this->get_shopper_infos();

            if ( is_array( $shopper_infos ) && count( $shopper_infos ) > 0 ) {
                $formatted = '';
                foreach ( $shopper_infos as $info_key => $info_el ) {
                    $formatted .= '&';
                    $formatted .= urlencode( $info_key );
                    $formatted .= '=';
                    $formatted .= urlencode( strval( $info_el ) );
                }
                return $formatted;
            }
            return '';

        }

        private function get_shipping_slot_infos_request( $cart_price, $email, $user_infos, $shipping_method_title, $shipping_carrier_title, $shipping_label, $shipping_reference, $shipping_price_reference ) {

            $body = array(   
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'lang' => get_locale(),
                'cart_price' => $cart_price,
                'currency_code' => get_woocommerce_currency(),
                'currency_symbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'email' => $email,
                'payload' => $user_infos,
                'shipping_method_title' => $shipping_method_title,
                'shipping_carrier_title' => $shipping_carrier_title,
                'shipping_label' => $shipping_label,
                'shipping_reference' => $shipping_reference,
                'shipping_price_reference' => $shipping_price_reference,
            );

            if ( isset( $this->moona_options['merchant_id'] ) ) {
                $body['merchant_id'] = $this->moona_options['merchant_id'];
            }

            $url = ( $this->moona_options['test_mode'] === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT;
            $url .= 'ecommerce/get_shipping_slot_infos';
            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $this->moona_options['test_mode'] === 'yes' ) ? $this->moona_options['moona_sk_test'] : $this->moona_options['moona_sk_live']
                    ),
                    'body' => json_encode($body)
                )
            );
            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
                $this->log( 'error on get_shipping_slot_infos() process' );
                return false;
            } else {
                $logger = wc_get_logger();
                $logger->info( wc_print_r( $response, true ), array( 'source' => 'moona' ) );

                $body = json_decode( $response['body'], true );

                if ( array_key_exists( 'call_on_update_shipping', $body['body'] ) ) {
                    $this->call_get_shipping_slot_infos_on_update = $body['body']['call_on_update_shipping'];
                    $_SESSION['moona_shipping_infos_refresh_shipping'] = $body['body']['call_on_update_shipping'];   
                }
                if ( array_key_exists( 'new_user', $body['body'] ) ) {
                    $this->shipping_infos_new_user = $body['body']['new_user'];
                    $_SESSION['moona_shipping_infos_new_user'] = $body['body']['new_user'];
                }
                if ( array_key_exists( 'returning_user', $body['body'] ) ) {
                    $this->shipping_infos_returning_user = $body['body']['returning_user'];
                    $_SESSION['moona_shipping_infos_returning_user'] = $body['body']['returning_user'];
                }
                return true;
            }
        }

        public function get_shipping_slot_infos() {

            if ( ! is_checkout() && ! is_cart() )
                return ;

            if ( $this->call_get_shipping_slot_infos_on_update == false && $this->shipping_infos_first_load_done == true )
                return ;

            $cart_price = round( number_format(WC()->cart->total, 2 , '.' , '' ) * 100);
            $email = WC()->customer->get_billing_email();
            $shopper_infos = $this->shopper_infos = $this->get_shopper_infos();

            $shipping_rate = false;
            $moona_options = get_option('woocommerce_discount-payment-moona_settings');
            if ( ! array_key_exists( 'shipping_slot_associated_method', $moona_options ) )
                return;

            $shipping_instance_id = $moona_options['shipping_slot_associated_method'];
            foreach( WC()->cart->get_shipping_packages() as $id => $package ) {
                if ( WC()->session->get( 'shipping_for_package_'.$id ) != NULL ) {
                    foreach( WC()->session->get( 'shipping_for_package_'.$id )['rates'] as $rate ) {
                        if ( $rate->instance_id == $shipping_instance_id ) {
                            $shipping_rate = $rate;
                            break;
                        }
                    }
                }
                break;
            }

            if ( $shipping_rate ) {
                // WC format
                // ( '_shipping_rate_id', $rate->id ); // the shipping rate ID (method_id:instance_id) => flat_rate:1
                // ( '_shipping_rate_method_id', $rate->method_id ); // the shipping rate method ID => flat_rate
                // ( '_shipping_rate_instance_id', (int) $rate->instance_id ); // the shipping rate instance ID => 1
                // ( '_shipping_rate_label', $rate->label ); // the shipping rate label => Flat rate 5
                // ( '_shipping_rate_cost', wc_format_decimal( (float) $rate->cost ) ); // the shipping rate cost => 500
                // ( '_shipping_rate_tax', wc_format_decimal( (float) array_sum( $rate->taxes ) ) ); // the shipping rate tax => 0

                $shippingCostTotal = (float) $shipping_rate->cost + (float) array_sum( $shipping_rate->taxes );
                // $shipping_method_title = $shipping_rate->label; // Flat rate 5
                // $shipping_carrier_title = $shipping_rate->method_id; // flat_rate
                $shipping_method_title = $shipping_rate->method_id; // flat_rate
                $shipping_carrier_title = $shipping_rate->label; // Flat rate 5
                $shipping_label = $moona_options['shipping_slot_label'];
                $shipping_reference = (int) $shipping_rate->id;
                $shipping_price_reference = round( number_format($shippingCostTotal, 2 , '.' , '' ) * 100);
            } else {
                $shipping_method_title = '';
                $shipping_carrier_title = '';
                $shipping_label = '';
                $shipping_reference = '';
                $shipping_price_reference = '';
            }

           $this->get_shipping_slot_infos_request( $cart_price, $email, $shopper_infos, $shipping_method_title, $shipping_carrier_title, $shipping_label, $shipping_reference, $shipping_price_reference );

           $this->shipping_infos_first_load_done = true;
           if ( session_status() === PHP_SESSION_NONE ) {
               session_start();
           }
           $_SESSION['moona_shipping_infos_first_load'] = true;
        }

        public function get_shipping_infos_new_user() {

            if ( ! $this->shipping_infos_new_user || $this->shipping_infos_first_load_done == false ) {
                $this->get_shipping_slot_infos();
            }

            return $this->shipping_infos_new_user;
        }

        public function get_shipping_infos_returning_user() {

            if ( ! $this->shipping_infos_returning_user || $this->shipping_infos_first_load_done == false ) {
                $this->get_shipping_slot_infos();
            }

            return $this->shipping_infos_returning_user;
        }

        public function is_mship_setting_enabled() {
           $moona_options = get_option('woocommerce_discount-payment-moona_settings');
           $mship_enabled = array_key_exists('shipping_slot_enabled', $moona_options) && $moona_options['shipping_slot_enabled'] === 'yes' ? true : false;
           return $mship_enabled; 
        }

        public function show_mship() {

            /* Conditions to show Shimp :
            1/ Mship is enabled in settings
            AND 2.1/ This is a new user (based on email)
            OR 2.2/ This is returning user + cart is over £XX
            AND 3/ display_slot in shipping_infos webservice equals true
            */

            if ( ! $this->is_mship_setting_enabled() ) {
                return false;
            }

            $shopper_infos = $this->get_shopper_infos();
            // if ( $shopper_infos == NULL ) {
            //     return $shipping_infos_new_user['display_slot'];
            // }

            if ( $shopper_infos != NULL && $this->is_returning_user() ) {
                $shipping_infos_returning_user = $this->get_shipping_infos_returning_user();
                if ( array_key_exists( 'sbfs', $shopper_infos ) && ( $shopper_infos['sbfs'] == 1 || $shopper_infos['sbfs'] == 2 ) ) {
                    $cart_total = round( number_format(WC()->cart->subtotal, 2 , '.' , '' ) * 100);
                    if ( $cart_total < $shipping_infos_returning_user['min_amount_to_display'] ) {
                        return false;
                    }
                }

                if ( $shipping_infos_returning_user['display_slot'] == false ) {
                    return false;
                }

            } else {
                $shipping_infos_new_user = $this->get_shipping_infos_new_user();
                if ( $shipping_infos_new_user['display_slot'] == false ) {
                    return false;
                }
                $cart_total = round( number_format(WC()->cart->subtotal, 2 , '.' , '' ) * 100);
                if ( $cart_total < $shipping_infos_new_user['min_amount_to_display'] ) {
                    return false;
                }
                if ( $shipping_infos_new_user['display_slot'] == false ) {
                    return false;
                }
            }

            return true;

        }

        public function is_mship_selected() {

            if ( WC()->session == NULL || WC()->session->get( 'chosen_shipping_methods' ) == NULL )
                return false;

            $chosen_shipping_method = WC()->session->get( 'chosen_shipping_methods' )[0];
            if ( $chosen_shipping_method != 'mship' )
                return false;
            return true;

        }

        public function is_mship_selected_in_order( $order_id ) {

            $order = wc_get_order( $order_id );

            if ( $order->get_meta( '_mship' ) == 1 || get_post_meta( $order_id, '_mship', true ) == 1 )
                return true;

            foreach( $order->get_items( 'shipping' ) as $item_id => $item ){
                $shipping_method_instance_id = $item->get_instance_id();
                if ( $shipping_method_instance_id == 999 )
                    return true;
            }

            return false;

        }

        public function get_mship_real_cost( $delivery_cost = false ) {
            if ( ! $delivery_cost ) {

                $shipping_rate = false;
                $chosen_shippings = WC()->session->get( 'chosen_shipping_methods' );
                    
                foreach( WC()->cart->get_shipping_packages() as $id => $package ) {
                    $chosen = $chosen_shippings[$id]; // The chosen shipping method
                    $shipping_rate = WC()->session->get('shipping_for_package_'.$id)['rates'][$chosen];
                    break;
                }

                if ( ! $shipping_rate )
                    return false;

                $delivery_cost = round( number_format($shipping_rate->cost, 2 , '.' , '' ) * 100);
            }

            $discount = $this->is_returning_user() ? $this->get_shipping_infos_returning_user()['discount'] : $this->get_shipping_infos_new_user()['discount'];
            if ( $delivery_cost - $discount > 0 ) {
                $new_cost = $delivery_cost - $discount;
                $new_cost = $new_cost / 100;
                return $new_cost;
            }
            return 0.00;
        }

        public function is_returning_user() {

            $shopper_infos = $this->get_shopper_infos();

            if ( $shopper_infos != NULL && array_key_exists( 'sbfs', $shopper_infos ) ) {
                return ( $shopper_infos['sbfs'] == 1 || $shopper_infos['sbfs'] == 2 );
            }

            return false;
        }


        public function disable_shipping_cache_conditionnal(){

            WC_Cache_Helper::get_transient_version( 'shipping', true );

            if ( is_admin() )
                return;

            $timestamp = get_option( 'moona_disable_cache_until_timestamp' );
            if ( $timestamp != false ) {
                if ( $timestamp > time() ) {
                    WC_Cache_Helper::get_transient_version( 'shipping', true );
                }   
            }

        }

        /* Create a "virtual" shipping method for Mship if enabled */
        public function add_shipping_slot( $rates, $packages ) {

            if ( ! is_array( $rates ) )
                return $rates;

            $moona_options = get_option('woocommerce_discount-payment-moona_settings');
            $show_mship = $this->show_mship();
            if ( ! $show_mship ) {
                return $rates;
            }

            if ( ! array_key_exists( 'shipping_slot_associated_method', $moona_options ) )
                return $rates;

            $shipping_instance_id = $moona_options['shipping_slot_associated_method'];
            $associated_shipping_id = false;
            foreach ( $rates as $rate_id => $rate ) {
                if ( $rate->instance_id == $shipping_instance_id ) {
                    $associated_shipping_id = $rate_id;
                    break;
                }
            }

            if ( $associated_shipping_id != false ) {
                $method_id = $rates[$associated_shipping_id]->get_method_id();
                $instance_id = 999;
                $rate_id = 'mship';

                $rates[$rate_id] = clone $rates[$associated_shipping_id]; // copy by value instead of reference
                $shipping_slot_label = $moona_options['shipping_slot_label'];

                if ( $this->shipping_infos_new_user == NULL || $this->shipping_infos_returning_user == NULL ) {
                    $this->get_shipping_slot_infos();
                }

                $initial_cost = round( number_format($rates[$rate_id]->get_cost(), 2 , '.' , '' ) * 100);
                $new_cost = $this->get_mship_real_cost( $initial_cost );
                $shipping_infos_returning_user = $this->get_shipping_infos_returning_user();
                $shipping_infos_new_user = $this->get_shipping_infos_new_user();
                if ( $this->is_returning_user() && $new_cost == 0.00 ) {
                    $shipping_slot_label = $shipping_infos_returning_user['title_free'];
                    if ( ! empty( $shipping_infos_new_user['description_free'] ) ) $shipping_slot_label .= ' - ' . $shipping_infos_new_user['description_free'];
                } else if ( $this->is_returning_user() && $new_cost > 0.00 ) {
                    $shipping_slot_label = $shipping_infos_returning_user['title'];
                    if ( ! empty( $shipping_infos_new_user['description'] ) ) $shipping_slot_label .= ' - ' . $shipping_infos_new_user['description'];
                } if ( ! $this->is_returning_user() && $new_cost == 0.00 ) {
                    $shipping_slot_label = $shipping_infos_new_user['title_free'];
                    if ( ! empty( $shipping_infos_new_user['description_free'] ) ) $shipping_slot_label .= ' - ' . $shipping_infos_new_user['description_free'];
                } else if ( ! $this->is_returning_user() && $new_cost > 0.00 ) {
                    $shipping_slot_label = $shipping_infos_new_user['title'];
                    if ( ! empty( $shipping_infos_new_user['description'] ) ) $shipping_slot_label .= ' - ' . $shipping_infos_new_user['description'];
                }
                $rates[$rate_id]->set_label( $shipping_slot_label );
                $rates[$rate_id]->set_cost( strval( $new_cost ) );
                $rates[$rate_id]->set_instance_id( $instance_id );
                
                $rates[$rate_id]->set_id( $rate_id );
               
            }

            $shipping_slot_sort = $moona_options['shipping_slot_sort'];
            if ( $shipping_slot_sort ) {
                $this->moveElement( $rates, count( $rates ) - 1, $shipping_slot_sort - 1); 
            }

            return $rates;
        }

        public function hide_associated_method() {

            if ( is_admin() || wp_doing_ajax() )
                return ;

            $moona_options = get_option('woocommerce_discount-payment-moona_settings');
            $shipping_slot_display_associated_method = $moona_options['shipping_slot_display_associated_method'];
            if ( $shipping_slot_display_associated_method == 'no' ) {
                $shipping_instance_id = $moona_options['shipping_slot_associated_method'];
                if ( WC()->cart ) {
                    $packages_keys = (array) array_keys(WC()->cart->get_shipping_packages());

                    foreach( $packages_keys as $id => $key ) {
                        if ( WC()->session && WC()->session->get('shipping_for_package_'.$key) != NULL ) {
                            $shipping_rates = WC()->session->get('shipping_for_package_'.$key)['rates'];
                            foreach( $shipping_rates as $rate_key => $rate ) {
                                if ( $rate->instance_id == $shipping_instance_id ) {
                                    $instance_id = $rate->instance_id;
                                    $method_id = $rate->method_id;
                                    ?>
                                    <style>
                                        input[type="radio"][value="<?php echo $method_id; ?>:<?php echo $instance_id; ?>"] {
                                            display: none !important;
                                        }
                                        label[for="shipping_method_<?php echo $id; ?>_<?php echo $method_id; ?><?php echo $instance_id; ?>"] {
                                            display: none !important;
                                        }
                                    </style>
                                    <?php
                                }
                            }
                        }
                    }
                }
            }

        }

        /* Refresh shipping cache when needed. Not working?? */
        public function woocommerce_shipping_rate_cache_invalidation( $packages) {

            return $packages;

            foreach ( $packages as &$package ) {
                $package['rate_cache'] = wp_rand();
            }

            return $packages;

        }

        /* Change elementor position in array
        $a = initial position
        $b = new position
        */
        private function moveElement( &$array, $a, $b ) {
            $keys = array_keys( $array );

            $this->custom_splice( $array, $a, $b );
            $this->custom_splice( $keys, $a, $b ); 

            $array = array_combine( $keys,$array );
        }

        private function custom_splice( &$ar, $a, $b ){
            $out = array_splice( $ar, $a, 1 );
            array_splice( $ar, $b, 0, $out );
        }

        /* Unset all payment gateways except Cleever's if Mship is enabled */
        public function show_active_gateways( $gateways ) {

            if ( is_admin() )
                return $gateways;

            if ( ! $this->is_mship_selected() )
                return $gateways;

            foreach ( $gateways as $gateway_id => $gateway_value ) {
                if ( ! in_array( $gateway_id, array( 'discount-payment-main-moona', 'discount-payment-moona' ) ) ) {
                    unset( $gateways[$gateway_id] );
                }
            }

            return $gateways;
        }

        public function mship_data_for_moona_session( $order_id ) {

            $order = wc_get_order( $order_id );
            $this->get_shopper_infos_webservice( $order->get_billing_email() );
            $shopper_infos = $this->get_shopper_infos();

            $is_returning_user = $this->is_returning_user();
            $shipping_infos = $is_returning_user ? $this->get_shipping_infos_returning_user() : $this->get_shipping_infos_new_user();

            $shipping_item = false;
            foreach( $order->get_items( 'shipping' ) as $item_id => $item ){
                $shipping_method_instance_id = $item->get_instance_id();
                if ( $shipping_method_instance_id == 999 || $order->get_meta( '_mship' ) == 1 || get_post_meta( $order_id, '_mship', true ) == 1 ) {
                    $shipping_item = $item;
                    break;
                }
            }

            if ( ! $shipping_item )
                return array();

            // Get the reference shipping method
            $moona_options = get_option('woocommerce_discount-payment-moona_settings');
            $shipping_reference_item = null;
            foreach( WC()->cart->get_shipping_packages() as $id => $package ) {
                if ( WC()->session->get( 'shipping_for_package_'.$id ) != NULL ) {
                    foreach( WC()->session->get( 'shipping_for_package_'.$id )['rates'] as $rate ) {
                        if ( $rate->instance_id == $moona_options['shipping_slot_associated_method'] ) {
                            $shipping_reference_item = $rate;
                            break;
                        }
                    }
                }
                break;
            }

            // $shipping_price_reference = round( number_format($shipping_item->get_total(), 2 , '.' , '' ) * 100);
            $shippingCostTotal = wc_format_decimal( (float) $shipping_reference_item->cost) + wc_format_decimal( (float) array_sum( $shipping_reference_item->taxes ));
            $shipping_price_reference = round( number_format($shippingCostTotal, 2 , '.' , '' ) * 100);
            if ($shipping_reference_item) {
                $shipping_method_title = $shipping_reference_item->method_id;
                $shipping_carrier_title = $shipping_reference_item->label;
                $shipping_reference = $shipping_reference_item->id;
            } else {
                $shipping_method_title = $shipping_item->get_method_title();
                $shipping_carrier_title = $shipping_item->get_method_id();
                $shipping_reference = $shipping_item->get_id();
            }

            $shipping_label = $moona_options['shipping_slot_label'];
            $discount = $shipping_infos['discount'];
            $min_amount_to_display = $shipping_infos['min_amount_to_display'];
            $title = $shipping_infos['title'];
            $description = $shipping_infos['description'];
            $subscription_status = array_key_exists( 'sbfs', $shopper_infos ) ? $shopper_infos['sbfs'] : 0;

            $data = array(
                'shipping_price_reference'  => $shipping_price_reference,
                'shipping_method_title'     => $shipping_method_title,
                'shipping_carrier_title'    => $shipping_carrier_title,
                'shipping_reference'        => $shipping_reference,
                'shipping_label'            => $shipping_label,
                'discount'                  => $discount,
                'min_amount_to_display'     => $min_amount_to_display,
                'title'                     => $title,
                'description'               => $description,
                'subscription_status'       => $subscription_status,
            );

            return $data;

        }

        /**
         * Add settings link
         */
        function add_action_settings_link( $links ){
            $action_links = array(
                'settings' => '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=discount-payment-moona' ) . '" aria-label="' . esc_attr__( 'View Cleever settings', 'discount-payment-moona' ) . '">' . esc_html__( 'Settings', 'discount-payment-moona' ) . '</a>',
            );
            return array_merge( $action_links, $links );
        }

        /**
         * Add notice
         */
        function wcmoona_woocommerce_fallback_notice()
        {
            $html = '<div class="notice notice-error">';
            $html .= '<p>' . __( 'The WooCommerce Cleever plugin requires the latest version of <a href="http://wordpress.org/extend/plugins/woocommerce/" target="_blank">WooCommerce</a> to work!', 'discount-payment-moona' ) . '</p>';
            $html .= '</div>';
            echo  $html ;
        }

        /**
         * Add Cleever gateway
         */
        function add_moona_gateway_class( $methods )
        {
            require_once WCMOONA_BASE_PATH . '/includes/class_wc_moona_gateway.php';
            $methods[] = 'WC_Moona_Gateway';

            require_once WCMOONA_BASE_PATH . '/includes/class_wc_moona_main_gateway.php';
            $methods[] = 'WC_Moona_Main_Gateway';

            return $methods;
        }

        /**
         * Logging method.
         *
         * @param string $message Log message.
         * @param string $level   Optional. Default 'alert'.
         *     emergency|alert|critical|error|warning|notice|info|debug
         */
        public static function log( $message, $level = 'alert' )
        {
            if ( ! class_exists( 'WC_Logger' ) ) {
                return;
            }

            if ( empty(self::$log) ) {
                self::$log = wc_get_logger();
            }
            self::$log->log( $level, $message, array(
                'source' => 'moona',
            ) );
        }

        /******************************************************************************/
        /*********************** CRON PENDING ORDERS ** *******************************/
        /******************************************************************************/

        /**
         * Add new 'every 10 minutes' schedule
         * @param $schedules
         * @return mixed
         */
        public function cron_every_ten_minutes( $schedules ) {
            // Add once ten_minutes to the existing schedules.
            if ( !isset($schedules["ten_minutes"]) ) {
                $schedules['ten_minutes'] = array(
                    'interval' => 600,
                    'display' => __( 'Every 10 minutes' )
                );
            }
            return $schedules;
        }

        /**
         * Create cron jobs to clear pending_orders
         */
        public function create_moona_cron_jobs() {
            wp_clear_scheduled_hook( 'moona_cancel_pending_orders' );

            if (!wp_next_scheduled('moona_cron')) {
                wp_schedule_event(time(), 'ten_minutes', 'moona_cron');
            }
        }

        /**
         * Cancel abandoned moona payment after 10 minutes
         */
        public function update_orders() {
            $orders = $this->get_pending_moona_orders();

            // Only pending orders
            foreach ($orders as $order) {
                $isPaid = $order->get_meta('_is_paid');

                // Add a note
                if ( $isPaid === 'true') {
                    $order->add_order_note( __( 'Order is paid but your server seems to stop the communication with Stripe. Please contact us through Cleever with the order id (merchant@moona.com).', 'discount-payment-moona'), false );
                    $order->update_status( 'on-hold', true );
                    // TODO : Later, check to refresh directly the IPN here
                } else {
                    wp_trash_post($order->get_id());
                }
            }
        }

        /**
         * Get created via moona orders
         * @return array
         */
        public function get_pending_moona_orders() {
            // Get 'pending payment' orders created via moona since more than 10 min
            $args = array(
                'limit' => -1,
                'type' => 'shop_order',
                // 'created_via' => 'moona-payment',
                'payment_method' => 'discount-payment-main-moona',
                'status' => array( 'pending' ),
                'date_created' => '<' . ( time() - 600 )
            );
            $ordersMoonaPayment = wc_get_orders( $args );

            // $args['created_via'] = 'moona-discount';
            $args['payment_method'] = 'discount-payment-moona';
            $ordersMoonaDiscount = wc_get_orders( $args );
            $results = array_merge($ordersMoonaPayment, $ordersMoonaDiscount);

            return $results;
        }

        /**********************************************************************************/
        /*********************** SINGLE PRODUCT CHECKBOX ** *******************************/
        /**********************************************************************************/

        // Add the checkbox above add to cart on single product template + send event to Amplitude
        public function single_product_checkbox() {

            $enabled = ( isset( $this->moona_options['product_slot_enabled'] ) && $this->moona_options['product_slot_enabled'] == 'yes' ) ? true : false;
            if ( $enabled == false )
                return;
                
            if ( isset( $_COOKIE['slpcbx'] ) )
                $to_display = false;
            else
                $to_display = true;

            $ws_data = $this->webservice_get_checkbox_data();

            if ( $to_display ) {
                ?>
                <div class="moona-sp-checkbox">
                    <div class="moona-spc-input-container">
                        <input type="checkbox" id="moona-spc-input" />
                        <label for="moona-spc-input"><?php echo $ws_data['title']; ?></label>
                    </div>
                    <div class="moona-spc-description" style="display: none;">
                        <?php echo $ws_data['description']; ?>
                    </div>
                </div>
                <?php
            }

            $shortVersion = explode(".", WC_VERSION);

            $eventProperties = array(
                'merchantSite' =>  get_bloginfo( 'name' ),
                'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                'currency' => get_woocommerce_currency(),
                'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                'pluginVersion' => WCMOONA_PLUGIN_VERSION,
                'title' => $ws_data['title'],
                'description' => $ws_data['description'],
                'discount' => $ws_data['discount'],
                'displayed' => $to_display,
            );

            if (isset($this->moona_options['merchant_id'])) {
                $eventProperties['merchantID'] = $this->moona_options['merchant_id'];
            }

            $GLOBALS['wc_moona_amplitude']->logEvent('View - Product page', $eventProperties);
        }

        // On product checkbox change, send new value to Amplitude
        public function product_checkbox_onchange_event() {

            $enabled = ( isset( $this->moona_options['product_slot_enabled'] ) && $this->moona_options['product_slot_enabled'] == 'yes' ) ? true : false;
            if ( $enabled == false )
                return;

            $checked = $_POST['checked'];

            $ws_data = $this->webservice_get_checkbox_data();

            $shortVersion = explode(".", WC_VERSION);

            $eventProperties = array(
                'merchantSite' =>  get_bloginfo( 'name' ),
                'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                'currency' => get_woocommerce_currency(),
                'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                'pluginVersion' => WCMOONA_PLUGIN_VERSION,
                'title' => $ws_data['title'],
                'description' => $ws_data['description'],
                'discount' => $ws_data['discount'],
                'checked' => $checked,
            );

            if (isset($this->moona_options['merchant_id'])) {
                $eventProperties['merchantID'] = $this->moona_options['merchant_id'];
            }

            $GLOBALS['wc_moona_amplitude']->logEvent('Click - Product page - Checkbox', $eventProperties);
        }

        // when product is added to cart, send data to Amplitude
        public function product_checkbox_added_to_cart() {

            $enabled = ( isset( $this->moona_options['product_slot_enabled'] ) && $this->moona_options['product_slot_enabled'] == 'yes' ) ? true : false;
            if ( $enabled == false )
                return;

            $checked = isset( $_COOKIE['slpcbx'] ) ? true : false;

            $ws_data = $this->webservice_get_checkbox_data();

            $shortVersion = explode(".", WC_VERSION);

            $eventProperties = array(
                'merchantSite' =>  get_bloginfo( 'name' ),
                'merchantURL' => get_permalink( wc_get_page_id( 'shop' ) ),
                'currency' => get_woocommerce_currency(),
                'currencySymbol' => (htmlentities(get_woocommerce_currency_symbol()) === '&amp;pound;') ? '£' : get_woocommerce_currency_symbol(),
                'ecommerceSolution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerceVersion' => WC_VERSION, // ex : 5.0.0
                'ecommerceShortVersion' => $shortVersion[0].'.'.$shortVersion[1], // ex : 5.0
                'pluginVersion' => WCMOONA_PLUGIN_VERSION,
                'title' => $ws_data['title'],
                'description' => $ws_data['description'],
                'discount' => $ws_data['discount'],
                'slotProductEnabled' => $enabled,
                'slotProductChecked' => $checked,
            );

            if (isset($this->moona_options['merchant_id'])) {
                $eventProperties['merchantID'] = $this->moona_options['merchant_id'];
            }

            $GLOBALS['wc_moona_amplitude']->logEvent('Click - Product page - Add to cart', $eventProperties);
        }

        // Get checkbox data from webservice
        private function webservice_get_checkbox_data() {

            if ( $this->product_slot_data != false )
                return $this->product_slot_data;

            global $product;
            if ( $product && is_a( $product, 'WC_Product' ) )
                $price = $product->get_price();
            else
                $price = 0;

            $body = array(
                'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
                'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
                'plugin_version' => WCMOONA_PLUGIN_VERSION,
                'api_version' => WCMOONA_API_VERSION,
                'currency' => get_woocommerce_currency(),
                'lang' => get_locale(),
                'price' => round( number_format( $price, 2 , '.' , '' ) * 100),
            );

            if ( isset( $this->moona_options['merchant_id'] ) ) {
                $body['merchant_id'] = $this->moona_options['merchant_id'];
            }

            $moona_options = get_option('woocommerce_discount-payment-moona_settings');

            $url = (( $moona_options['test_mode'] === 'yes' ) ? MOONA_API_STAGING_ENDPOINT : MOONA_API_ENDPOINT);
            $url .= 'ecommerce/product_page_offer';

            $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 20,
                    'headers' => array(
                        'Content-Type' => 'application/json; charset=utf-8',
                        'x-api-key' => ( $moona_options['test_mode'] ) ? $moona_options['moona_sk_test'] : $moona_options['moona_sk_live'],
                    ),
                    'body' => json_encode($body)
                )
            );

            if ( session_status() === PHP_SESSION_NONE ) {
                session_start();
            }

            if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
                $this->log( 'error on customize_checkout_infos() process : ' . print_r($response, true) );
                $default = $this->webservice_get_checkbox_data_default();
                $this->product_slot_data = $default;
                $_SESSION['product_slot_data'] = $default;
                return $default;
            } else {
                $body = json_decode( $response['body'], true );
                $this->product_slot_data = $body;
                $_SESSION['product_slot_data'] = $body;
                return $body;
            } 

        }

        // Default values when webservice call fails
        private function webservice_get_checkbox_data_default() {
            return array(
                'title' => '<b>Apply £5 discount now</b>',
                'description' => 'Get £5 now and unlock 100\'s of offers. 7 day FREE trial, then £29.99 per month. Cancel anytime.',
                'discount' => 500,
            );
        }

        // Select MP1 as default gateway if checkbox has been checked
        public function checkbox_set_mp1_as_default_gateway() {
            if( is_checkout() && ! is_wc_endpoint_url() && isset( $_COOKIE['slpcbx'] ) ) {
                $default_payment_id = 'discount-payment-main-moona';
                WC()->session->set( 'chosen_payment_method', $default_payment_id );
            }
        }

        // Remove cookie after order has been paid
        public function unset_checkbox_cookie_after_order( $order_id ) {
            if ( isset( $_COOKIE['slpcbx'] ) ) {
                unset( $_COOKIE['slpcbx'] );
                setcookie( 'slpcbx', '', time() - 3600, '/' );
            }
        }

        // Remove cookie if payment is cancelled with MP2
        public function unset_cookie_if_mp2_cancel( $order_id ) {
            if ( is_cart() && isset( $_GET['slpcbx'] ) && $_GET['slpcbx'] == 0 ) {
                if ( isset( $_COOKIE['slpcbx'] ) ) {
                    unset( $_COOKIE['slpcbx'] );
                    setcookie( 'slpcbx', '', time() - 3600, '/' );
                }   
            }
        }

        // Update total in cart
        public function update_cart_total_html_checkbox_product( $value ) {
            if ( is_cart() && isset( $_COOKIE['slpcbx'] ) ) {
                $total = WC()->cart->get_total( 'edit' );
                $total = ( $total * 100 ) - intval( $_COOKIE['slpcbx'] );
                $total = $total / 100;
                if ( $total <= 0 )
                    $total = 0;
                $discount_total = wc_price( $total );

                $cart_fulltotal = (float)WC()->cart->get_cart_contents_total() + (float)WC()->cart->get_taxes_total() + (float)WC()->cart->get_shipping_total();
                $value = sprintf( '<span class="moona-total-before-discount">%s</span> <strong class="moona-amount">%s</strong>', wc_price($cart_fulltotal), $discount_total );
            }
            return $value;
        }

        // Update subtotal in minicart
        public function update_minicart_subtotal_html_checkbox_product() {
            if ( isset( $_COOKIE['slpcbx'] ) ) {
                $subtotal = WC()->cart->get_subtotal();
                $subtotal = ( $subtotal * 100 ) - intval( $_COOKIE['slpcbx'] );
                $subtotal = $subtotal / 100;
                if ( $subtotal <= 0 )
                    $subtotal = 0;
                $discount_subtotal = wc_price( $subtotal );
                echo '<strong>' . esc_html__( 'Subtotal:', 'woocommerce' ) . '</strong> <span class="moona-total-before-discount">' . WC()->cart->get_cart_subtotal() . '</span> <strong class="moona-amount">' . $discount_subtotal . '</strong>';
            } else {
                echo '<strong>' . esc_html__( 'Subtotal:', 'woocommerce' ) . '</strong>' . WC()->cart->get_cart_subtotal();
            }
            
        }

        public function checkbox_product_filter_html_price( $price, $product ){

            if ( ! is_singular( 'product' ) )
                return $price;

            $enabled = ( isset( $this->moona_options['product_slot_enabled'] ) && $this->moona_options['product_slot_enabled'] == 'yes' ) ? true : false;
            if ( $enabled == false )
                return $price;

            if ( isset( $_COOKIE['slpcbx'] ) )
                return $price;

            if ( ! array_key_exists( 'discount', $this->product_slot_data ) )
                return $price;

            $discount = $this->product_slot_data['discount'];

            if ( $discount <= 0 )
                return $price;

            if ( $product->is_type( 'simple' ) || $product->is_type( 'variation' ) || $product->is_type( 'external' ) ) {
                $sale_price = get_post_meta( $product->get_id(), '_sale_price', true);
                if ( $sale_price ) {
                    $new_price = ( $sale_price * 100 - $discount ) / 100;
                    if ( $new_price <= 0 )
                        $new_price = 0;
                    $price = '<span class="moona-total-before-discount">' . $price . '</span>';
                    $price .= ' <span class="moona-amount">' . wc_price( $new_price ) . '</span>';
                } else {
                    $new_price = ( $product->get_price() * 100 - $discount ) / 100;
                    if ( $new_price <= 0 )
                        $new_price = 0;
                    $price = '<span class="moona-total-before-discount">' . $price . '</span>';
                    $price .= ' <span class="moona-amount">' . wc_price( $new_price ) . '</span>';
                }
            }
            else if ( $product->is_type( 'variable' ) ) {
                $min_price = $product->get_variation_price();
                $max_price = $product->get_variation_price('max');
                $min_price =  ( $min_price * 100 - $discount ) / 100;
                if ( $min_price <= 0 )
                    $min_price = 0;
                $max_price =  ( $max_price * 100 - $discount ) / 100;
                if ( $max_price <= 0 )
                    $max_price = 0;

                $price = '<span class="moona-total-before-discount">' . $price . '</span>';
                $price .= ' <span class="moona-amount">' . wc_price( $min_price ) . ' - ' . wc_price( $max_price ) . '</span>';
            }

            return $price;

        }

        function checkbox_product_filter_html_price_grouped( $price, $instance, $child_prices ) { 

            if ( ! is_singular( 'product' ) )
                return $price;

            $enabled = ( isset( $this->moona_options['product_slot_enabled'] ) && $this->moona_options['product_slot_enabled'] == 'yes' ) ? true : false;
            if ( $enabled == false )
                return $price;

            if ( isset( $_COOKIE['slpcbx'] ) )
                return $price;

            if ( ! array_key_exists( 'discount', $this->product_slot_data ) )
                return $price;

            $discount = $this->product_slot_data['discount'];

            if ( $discount <= 0 )
                return $price;

            $min_price = min( $child_prices );
            $max_price = max( $child_prices );
            $min_price =  ( $min_price * 100 - $discount ) / 100;
            if ( $min_price <= 0 )
                $min_price = 0;
            $max_price =  ( $max_price * 100 - $discount ) / 100;
            if ( $max_price <= 0 )
                $max_price = 0;

            $price = '<span class="moona-total-before-discount">' . $price . '</span>';
            $price .= ' <span class="moona-amount">' . wc_price( $min_price ) . ' - ' . wc_price( $max_price ) . '</span>';

            return $price; 
        } 

    }

    /******************************************************************************/
    /********************** ACTIVATION/DEACTIVATION *******************************/
    /******************************************************************************/

    function wcmoona_plugin_status($status) {

        $body = array(
            'ecommerce_solution' => WCMOONA_ECOMMERCE_SOLUTION,
            'ecommerce_version' => ( defined( 'WC_VERSION' ) ) ? WC_VERSION : '',
            'plugin_version' => WCMOONA_PLUGIN_VERSION,
            'api_version' => WCMOONA_API_VERSION,
            'status' => $status,
            'device_id' => wp_get_session_token()
        );

        $headers = array( 'Content-Type' => 'application/json; charset=utf-8' );

        $moona_options = get_option('woocommerce_discount-payment-moona_settings');
        if ($moona_options && ($status === 'enabled' || $status === 'disabled' || $status === 'updated')) {

            $body['options'] = json_encode($moona_options);

            $paymentKeys = array_keys( WC()->payment_gateways->get_available_payment_gateways() );
            $moonaDiscountPosition = array_search( 'discount-payment-moona',  $paymentKeys) ;
            $moonaPaymentPosition = array_search( 'discount-payment-main-moona', $paymentKeys) ;
            $moonaDiscountPosition = ($moonaDiscountPosition !== false) ? $moonaDiscountPosition : -1;
            $moonaPaymentPosition = ($moonaPaymentPosition !== false) ? $moonaPaymentPosition : -1;

            $body['payment_position'] = ($moonaDiscountPosition !== false) ? $moonaDiscountPosition : -1;
            $body['payment_position_main'] = ($moonaPaymentPosition !== false) ? $moonaPaymentPosition : -1;

            $shipping_enabled = $moona_options['shipping_slot_enabled'] === 'yes' ? true : false;
            if ( $shipping_enabled ) {
                $shipping_instance_id = $moona_options['shipping_slot_associated_method'];
                $zones = WC_Shipping_Zones::get_zones();
                $shipping_methods = array_column( $zones, 'shipping_methods' );
                foreach ( $shipping_methods[0] as $key => $class ) {
                    if ( $class->instance_id == $shipping_instance_id ) {
                        $body['options']['shipping_slot_associated_method_label'] = $class->title;
                        break;
                    }
                }
            }
            
            if ( isset( $moona_options['merchant_id'] ) ) {
                $body['merchant_id'] = $moona_options['merchant_id'];
            }

            // NOTE : CHANGE BY PROD ON RELEASE
            // STAGING
            // $headers['x-api-key'] = $moona_options['moona_sk_test'];
            // PROD
            $headers['x-api-key'] = $moona_options['moona_sk_live'];
        }
        else {
            $body['merchant_email'] = (get_option('admin_email')) ? get_option('admin_email') : '';
            $body['merchant_fullname'] = (get_option('blogname')) ? get_option('blogname') : '';
            $body['merchant_website'] = (get_site_url()) ? get_site_url() : ''; // get_option('home') or get_option('siteurl')
        }

        // NOTE : CHANGE BY PROD ON RELEASE
        // STAGING
        // $url = MOONA_API_STAGING_ENDPOINT;
        // PROD
        $url = MOONA_API_ENDPOINT;

        $url .= 'ecommerce/status';
        // error_log(print_r('wcmoona_plugin_activated process : ' . json_encode($body), true));

        $response = wp_remote_post( $url, array(
                'method' => 'POST',
                'timeout' => 20,
                'headers' => $headers,
                'body' => json_encode($body)
            )
        );

        if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
            error_log(print_r('error on wcmoona_plugin_status()' . $status . ' process : ' . print_r($response, true), true));
            return $response;
        } else {
            $body = json_decode( $response['body'], true );
            if ( array_key_exists ( 'merchant_id', $body ) && $body['merchant_id'] != $moona_options['merchant_id'] ) {
                $moona_options['merchant_id'] = $body['merchant_id'];
                update_option( 'woocommerce_discount-payment-moona_settings', $moona_options );
            }
            return $body;
        }
    }

    /**
     * No x-api-key yet
     * Status : installed
     */
    function wcmoona_plugin_activated() {
        wcmoona_plugin_status('installed');
    }

    /**
     * No x-api-key yet
     * Status : uninstalled
     */
    function wcmoona_plugin_deactivated() {
        wcmoona_plugin_status('uninstalled');
    }

    /**
     * No x-api-key yet
     * Status : uninstalled
     */
    function wcmoona_plugin_uninstall() {
        wcmoona_plugin_status('uninstalled');
    }

    // register_activation_hook( __FILE__,  array( $this, 'wcmoona_plugin_activated'), 0 );
    register_activation_hook( __FILE__,  'wcmoona_plugin_activated');
    register_deactivation_hook( __FILE__,  'wcmoona_plugin_deactivated');
    register_uninstall_hook( __FILE__,  'wcmoona_plugin_uninstall');

    /**
     * On Cleever upgrade
     */
    function upgrade_moona_plugin( $upgrader_object, $options ) {
        // The path to our plugin's main file
        $moona_plugin = plugin_basename( __FILE__ );
        $moona_updated = false;

        // If an update has taken place and the updated type is plugins and the plugins element exists
        if( $options['action'] == 'update' && $options['type'] == 'plugin' && isset( $options['plugins'] ) ) {
            // Iterate through the plugins being updated and check if ours is there
            foreach( $options['plugins'] as $plugin ) {
                if( $plugin == $moona_plugin ) {

                    $moona_updated = true;

                    // Set a transient to record that our plugin has just been updated
                    // set_transient( 'wp_upe_updated', 1 );
                    break;

                    // Check the transient to see if we've just updated the plugin
                    // if( get_transient( 'wp_upe_updated' ) ) {
                    //     echo '<div class="notice notice-success">' . __( 'Thanks for updating', 'wp-upe' ) . '</div>';
                    //     delete_transient( 'wp_upe_updated' );
                    // }
                }
            }
        }

        if ( ! $moona_updated ) {
            return;
        }

        // Send the update info
        wcmoona_plugin_status('updated');
    }

    add_action( 'upgrader_process_complete', 'upgrade_moona_plugin',10, 2);

}

// Instantiate the plugin class and add it to the set of globals
$GLOBALS['wc_moona'] = WC_Moona::get_instance();