jQuery(function($) {

    var current_payment_method;

    // 1 - Amplitude call when we change the payment method
    // 2 - Refresh the checkout to display discount at checkout
    var wc_checkout_form = {
        $checkout_form: $('form.woocommerce-checkout'),
        init: function () {
            // For the first call
            $(document.body).on('updated_checkout', this.do_update_checkout);

            // Payment method selection updated
            this.$checkout_form.on('change', 'input[name^="payment_method"]', this.trigger_update_payment_method);
        },
        do_update_checkout: function (event, args) {
            const payment_method_id = $("input[name='payment_method']:checked").val();
            const payment_method_label = $("label[for='payment_method_" + payment_method_id + "']").text().trim();

            current_payment_method = payment_method_id;

            // AMPLI : Send infos on payment method modification
            var data = {
                action: 'amplitude_action',
                step: 'change_payment_method',
                payment_method_id: payment_method_id,
                payment_method_label: payment_method_label
            };

            $.post(ajax_object.ajax_url, data); // jQuery.post
        },
        trigger_update_payment_method: function (event, args) {
            // Refresh the checkout
            $(document.body).trigger("update_checkout");
        },
    }

    var wc_handle_submission = {
        $checkout_form : $('form.woocommerce-checkout'),

        init: function () {
            // Fires button clic handle
            this.$checkout_form.on('checkout_place_order', this.do_handle_button);

            window.addEventListener("message", (event) => {
                 if (!event.origin.includes("secure-payment-ecommerce.com") && !event.origin.includes("secure-payment.moona.com") && !event.origin.includes("e-securepay.com") && !event.origin.includes("localhost"))
                     return;
                     
                var data = event.data;
                if (data.source !== "Moona" && data.source !== "Cleever") {
                    // console.log("Cleever checkout: Wrong source");
                    return;
                } else {
                    // console.log("MOONA.JS");

                    switch (data.action) {
                        case "form-validation:valid":

                            // Keep the payload
                            this.$checkout_form.find('#payment_payload').val(JSON.stringify(data.payload));

                            // Deactivate the do_handle_button function event
	                        this.$checkout_form.off( 'checkout_place_order', this.do_handle_button );

                            // Submit the form
                            this.$checkout_form.submit();

                            break;
                        case "form-validation:errors":
                            // // let height = this.$checkout_form.find('#payment-integrated-container').height();
                            // height = 240 + (data.payload.errors.length * 20);
                            // // if (this.$checkout_form.find('#payment-integrated-container').css("height") != "300px") { 
                            //     this.$checkout_form.find('#payment-integrated-container').css('height', height + 'px');
                            //     this.$checkout_form.find('#payment-integrated-container > div').css('height', height + 'px');
                            // // }
                            break;
                        case "form-validation:no-error":
                            // if (this.$checkout_form.find('#payment-integrated-container').css("height") != "240px") { 
                            //     this.$checkout_form.find('#payment-integrated-container').css('height', '240px');
                            //     this.$checkout_form.find('#payment-integrated-container > div').css('height', '240px');
                            // }
                            break;
                        case "form:resize":
                            this.$checkout_form.find('#payment-integrated-container').css('height', data.payload.height);
                            this.$checkout_form.find('#payment-integrated-container > div').css('height', data.payload.height);
                            break;
                        case 'form-cb:state':
                            if (data.payload.type === 'discount') {
                                var data = {
                                    action: 'moona_store_payload',
                                    payload: data.payload,
                                };

                                if (data.payload.is_checked == false) {
                                    eraseCookie('slpcbx');
                                }

                                $.post(ajax_object.ajax_url, data, function( response ) {
                                    var html = response.data.html;
                                    if (html == '') {
                                        jQuery('.woocommerce-checkout-review-order-table tfoot .new-order-total').remove();
                                        jQuery('.woocommerce-checkout-review-order-table tr.order-total:nth-of-type(3) td').css('text-decoration', 'initial');
                                        jQuery('.woocommerce-checkout-review-order-table tr.order-total:nth-of-type(3) td > strong').css('text-decoration', 'none');
                                        jQuery('.woocommerce-checkout-review-order-table tr.order-total:nth-of-type(3) td > strong > span').css('text-decoration', 'none');
                                    } else {
                                        jQuery('.woocommerce-checkout-review-order-table tfoot .new-order-total').remove();
                                        jQuery('.woocommerce-checkout-review-order-table tfoot').append(html);
                                        // jQuery('.woocommerce-checkout-review-order-table tr.order-total:nth-of-type(3) td').css('text-decoration', 'line-through');
                                        jQuery('.woocommerce-checkout-review-order-table tr.order-total:nth-of-type(3) td > strong').css('text-decoration', 'line-through');
                                    }                            
                                });
                            }
                            break;
                        default:
                            console.log("There is no response of Cleever checkout API");
                    }
                }
            }, false);
        },

        do_handle_button: function () {
            if (current_payment_method === "discount-payment-main-moona") {
                // Send the click to the iframe and catch the click event to avoid to launch it now
                try {
                    const message = { action: 'cta:click', source: 'Moona' };
                    let moonaIframe = document.querySelector('#payment-form-integrated').contentWindow;
                    moonaIframe.postMessage(message, '*');
                } catch {
                    return false;
                }
                return false;
            } else {
                return true;
            }
        },
    }

    wc_handle_submission.init();
    wc_checkout_form.init();

    function validateEmail(input) {
        var validRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
        return input.match(validRegex);
    };

    //http://davidwalsh.name/javascript-debounce-function
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    jQuery('#customer_details #billing_email').keydown(
        debounce(function(event) {
            if ((event.which < 46 && event.which !== 8) || event.which === 91 || event.which === 92 || event.which === 93 || (event.which > 106 && event.which !== 188 && event.which !== 189 && event.which !== 190 && event.which !== 192)) {
                // NOTHING
            }
            else {
                const email = jQuery('#customer_details #billing_email').val();
                if (email && validateEmail(email)) {
                    var data = {
                        action: 'get_shopper_infos_ajax',
                        email: jQuery('#customer_details #billing_email').val(),
                    };

                    $.post(ajax_object.ajax_url, data, function( response ) {
                        jQuery(document.body).trigger("update_checkout");
                    });
                }
            }
        }, 1000)
    );

    function compute_new_price(checked) {
        jQuery('.moona-amount').remove();
        if (checked) {

            if (ajax_object.product_slot && ajax_object.product_slot.discount) {
                var discount = ajax_object.product_slot.discount;
            } else {
                var discount = 0;
            }

            if (jQuery('div.type-product').hasClass('product-type-variable')) {

                let total = jQuery('.summary p.price > span:not(.moona-amount):first-of-type bdi').text();
                total = total.replace('£', '');
                total = parseFloat(total);
                total = ( ( total * 100 ) - discount ) / 100;
                if (total < 0)
                 total = 0;
                total = total.toFixed(2);
                let total_html = ' <span class="woocommerce-Price-amount amount moona-amount"><bdi><span class="woocommerce-Price-currencySymbol">£</span>' + total + '</bdi></span>';
                jQuery('.summary p.price > span:not(.moona-amount):nth-of-type(2)').after(total_html);
                jQuery('.summary p.price > span:not(.moona-amount):first-of-type').addClass('moona-total-before-discount'); 

                total = jQuery('.summary p.price > span:not(.moona-amount):nth-of-type(2) bdi').text();
                total = total.replace('£', '');
                total = parseFloat(total);
                total = ( ( total * 100 ) - discount ) / 100;
                if (total < 0)
                 total = 0;
                total = total.toFixed(2);
                total_html = '<span class="woocommerce-Price-amount amount moona-amount"><bdi> – <span class="woocommerce-Price-currencySymbol">£</span>' + total + '</bdi></span>';
                jQuery('.summary p.price .moona-amount').after(total_html);
                jQuery('.summary p.price > span:not(.moona-amount):nth-of-type(2)').addClass('moona-total-before-discount'); 

                if (jQuery('.single_variation_wrap span.price ins').length > 0) {
                    let total = jQuery('.single_variation_wrap ins .woocommerce-Price-amount:not(.moona-amount) bdi').text();
                    total = total.replace('£', '');
                    total = parseFloat(total);
                    total = ( ( total * 100 ) - discount ) / 100;
                    if (total < 0)
                     total = 0;
                    total = total.toFixed(2);
                    let total_html = ' <span class="woocommerce-Price-amount amount moona-amount"><bdi><span class="woocommerce-Price-currencySymbol">£</span>' + total + '</bdi></span>';
                    jQuery('.single_variation_wrap ins .woocommerce-Price-amount').after(total_html);
                    jQuery('.single_variation_wrap ins .woocommerce-Price-amount:not(.moona-amount)').addClass('moona-total-before-discount'); 
                } else {
                    let total = jQuery('.single_variation_wrap .woocommerce-Price-amount:not(.moona-amount) bdi').text();
                    total = total.replace('£', '');
                    total = parseFloat(total);
                    total = ( ( total * 100 ) - discount ) / 100;
                    if (total < 0)
                     total = 0;
                    total = total.toFixed(2);
                    let total_html = ' <span class="woocommerce-Price-amount amount moona-amount"><bdi><span class="woocommerce-Price-currencySymbol">£</span>' + total + '</bdi></span>';
                    jQuery('.single_variation_wrap .woocommerce-Price-amount').after(total_html);
                    jQuery('.single_variation_wrap .woocommerce-Price-amount:not(.moona-amount)').addClass('moona-total-before-discount');               
                }

            } else if (jQuery('.entry-summary p.price ins').length > 0) {
               let total = jQuery('.summary ins .woocommerce-Price-amount:not(.moona-amount) bdi').text();
               total = total.replace('£', '');
               total = parseFloat(total);
               total = ( ( total * 100 ) - discount ) / 100;
               if (total < 0)
                total = 0;
               total = total.toFixed(2);
               let total_html = ' <span class="woocommerce-Price-amount amount moona-amount"><bdi><span class="woocommerce-Price-currencySymbol">£</span>' + total + '</bdi></span>';
               jQuery('.summary ins .woocommerce-Price-amount').after(total_html);
               jQuery('.summary ins .woocommerce-Price-amount:not(.moona-amount)').addClass('moona-total-before-discount'); 
            } else {
                let total = jQuery('.summary .woocommerce-Price-amount:not(.moona-amount) bdi').text();
                total = total.replace('£', '');
                total = parseFloat(total);
                total = ( ( total * 100 ) - discount ) / 100;
                if (total < 0)
                 total = 0;
                total = total.toFixed(2);
                let total_html = ' <span class="woocommerce-Price-amount amount moona-amount"><bdi><span class="woocommerce-Price-currencySymbol">£</span>' + total + '</bdi></span>';
                jQuery('.summary .woocommerce-Price-amount').after(total_html);
             jQuery('.summary .woocommerce-Price-amount:not(.moona-amount)').addClass('moona-total-before-discount');               
            }

        } else {
            jQuery('.summary .woocommerce-Price-amount:not(.moona-amount)').removeClass('moona-total-before-discount');
        }
    }

    jQuery('#moona-spc-input').on('change', function() {
        let checked = jQuery('#moona-spc-input').is(':checked');

        if (checked) {
            jQuery('.moona-spc-description').show();
            jQuery('.summary p.price').addClass('show-discount');
            jQuery('.product-type-variable').addClass('show-discount');
           // compute_new_price(true);
        } else {
          jQuery('.moona-spc-description').hide();
          //  compute_new_price(false);
          jQuery('.summary p.price').removeClass('show-discount');
          jQuery('.product-type-variable').removeClass('show-discount');
        }

        var data = {
            action: 'product_checkbox_onchange_event',
            checked: checked,
        };

        $.post(ajax_object.ajax_url, data, function( response ) {
        });
    });

    jQuery('.single-product .single_add_to_cart_button').on('click', function() {
        if (jQuery('#moona-spc-input').is(':checked')) {
            if (ajax_object.product_slot && ajax_object.product_slot.discount)
                createCookie('slpcbx', ajax_object.product_slot.discount, 3);
            else
                createCookie('slpcbx', 0, 3);
        }
    });

    function createCookie(name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = encodeURIComponent(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0)
                return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name, "", -1);
    }

});